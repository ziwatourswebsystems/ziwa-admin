-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 05, 2015 at 12:29 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ziwatour_ziwadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `blog_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(500) NOT NULL,
  `blog_image` varchar(255) NOT NULL,
  `blog_body` text NOT NULL,
  `blog_summary` varchar(1000) NOT NULL,
  `blog_tags` varchar(500) NOT NULL,
  `blog_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `blog_title`, `blog_image`, `blog_body`, `blog_summary`, `blog_tags`, `blog_date_created`) VALUES
(2, 'Just Say No to More End-to-End Tests', 'ziwatours.png', 'At some point in your life, you can probably recall a movie that you and your friends all wanted to see, and that you and your friends all regretted watching afterwards. Or maybe you remember that time your team thought they’d found the next "killer feature" for their product, only to see that feature bomb after it was released. \r\n\r\nGood ideas often fail in practice, and in the world of testing, one pervasive good idea that often fails in practice is a testing strategy built around end-to-end tests. \r\n\r\nTesters can invest their time in writing many types of automated tests, including unit tests, integration tests, and end-to-end tests, but this strategy invests mostly in end-to-end tests that verify the product or service as a whole. Typically, these tests simulate real user scenarios.\r\n                                    ', 'At some point in your life, you can probably recall a movie that you and your friends all wanted to see, and that you and your friends all regretted watching afterwards. Or maybe you remember that time your team thought they’d found the next "killer feature" for their product, only to see that feature bomb after it was released. \r\n                                    ', 'News,Stories,Exclusive', '2015-05-05 17:18:07'),
(3, 'How to Test Blog Post Titles With Twitter', 'how-to-pose.png', 'Are you choosing the best titles for your blog posts?\r\n\r\nWould you like an easy way to test headlines before you publish?\r\n\r\nGood titles lead to higher engagement and more click-throughs.                ', 'Are you choosing the best titles for your blog posts?\r\n\r\nWould you like an easy way to test headlines before you publish?\r\n                                    ', 'Tour,Stories,Blog', '2015-05-13 09:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `booking_transaction`
--

CREATE TABLE IF NOT EXISTS `booking_transaction` (
  `booking_transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `booking_type_id` tinyint(2) unsigned NOT NULL,
  `booking_id` tinyint(4) unsigned NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `date_booked_for` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `adults` tinyint(2) NOT NULL DEFAULT '1',
  `children` tinyint(2) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `notes` text NOT NULL,
  `transaction_token` varchar(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`booking_transaction_id`),
  KEY `booking_id` (`booking_id`,`user_id`,`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `booking_transaction`
--

INSERT INTO `booking_transaction` (`booking_transaction_id`, `booking_type_id`, `booking_id`, `user_id`, `date_booked_for`, `adults`, `children`, `status`, `notes`, `transaction_token`, `date_created`) VALUES
(1, 1, 1, 'deanvann@gmail.com', '2015-04-19 16:40:22', 1, 0, 1, '', '38pcfhTbOoy1UnvZC0vh', '2015-04-17 19:27:55'),
(2, 2, 3, 'deanvann@gmail.com', '2015-04-19 17:44:53', 1, 0, 0, '', 'C5Ba6Kh9kh3OL4LNFBCK', '2015-04-17 19:34:49'),
(3, 2, 4, 'deanvann@gmail.com', '2015-06-29 22:31:31', 1, 0, 3, '', '3T5CThDoS9DUkJBjhcCi', '2015-04-17 19:54:28'),
(4, 4, 1, 'deanvann@gmail.com', '2015-05-16 20:32:59', 1, 0, 4, '', 'BSSSoXBq1DULIWaSCSrL', '2015-04-17 20:07:18'),
(5, 4, 1, 'deanvann@gmail.com', '2015-02-01 22:00:00', 1, 0, 0, '', 'BSSSoXBq1DULIWaSC', '2015-04-17 20:07:18'),
(6, 1, 1, 'deanvann@gmail.com', '2015-04-22 22:00:00', 1, 0, 0, '', 'C5Bakh3OL4LNFBCK', '2015-04-30 19:34:49'),
(7, 3, 1, 'deanvann@gmail.com', '2015-05-05 22:00:00', 1, 0, 0, '', 'WHAysFFV9BjwIVKJ4AM1', '2015-05-06 18:29:49'),
(8, 3, 1, 'deanvann@gmail.com', '2015-05-12 22:00:00', 1, 0, 0, '', 'tFyzKnVg0SNebevz96mf', '2015-05-06 18:38:11'),
(9, 1, 1, 'john@nsow.com', '2015-05-05 22:00:00', 1, 0, 0, '', 'O0m7sRzexAN4IKnPTUWS', '2015-05-06 20:26:21'),
(10, 1, 1, 'deanvann@gmail.com', '2015-05-30 22:00:00', 1, 0, 0, '', 'tFHYe0KVKD0RARgroQ4T', '2015-05-06 20:29:39'),
(11, 1, 1, 'info@ziwatours.biz', '2015-05-25 22:00:00', 1, 0, 0, '', 'W3y1MMA2wUguJCmwUOmq', '2015-05-06 20:30:36'),
(12, 3, 2, 'deanvann@gmail.com', '2015-05-15 22:00:00', 1, 0, 0, '', 'Fv6iflL3PANM7Ht1g24s', '2015-05-16 21:38:10'),
(13, 1, 1, 'deanvann@gmail.com', '2015-05-23 22:00:00', 1, 0, 0, '', 'RNyBK6Y3doALV1PX7SUI', '2015-05-16 21:49:11'),
(14, 4, 1, 'deanvann@gmail.com', '2015-05-23 22:00:00', 1, 0, 0, '', 'HQ3jYapsWRT9DI0B5FrS', '2015-05-16 21:52:43'),
(15, 4, 1, 'deanvann@gmail.com', '2015-05-23 22:00:00', 1, 0, 0, '', '0Q1inw7mseFWgSNhotHz', '2015-05-16 21:53:02'),
(16, 4, 1, 'deanvann@gmail.com', '2015-05-23 22:00:00', 1, 0, 0, '', 'GR4nmNPX2F3TlSZfbtWk', '2015-05-16 21:54:49'),
(17, 3, 14, 'jcicapetown2015@gmail.com', '2015-05-30 04:00:00', 1, 0, 0, '', 'WLCChaZy9sS2U85ZNEIt', '2015-05-21 17:01:32'),
(18, 3, 7, 'Zabrave7@gmail.com', '2015-07-01 04:00:00', 1, 0, 0, '', 'XhWrqqeZEw4zQyuNcN7v', '2015-06-29 22:26:44'),
(19, 3, 7, 'Zabrave7@gmail.com', '2015-07-01 04:00:00', 1, 0, 0, '', 'pxtqmgG4f2Ub10CNpnyH', '2015-06-29 22:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `booking_types`
--

CREATE TABLE IF NOT EXISTS `booking_types` (
  `booking_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_type_name` varchar(50) NOT NULL,
  `booking_from` varchar(10) NOT NULL COMMENT 'tour or service',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`booking_type_id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `booking_types`
--

INSERT INTO `booking_types` (`booking_type_id`, `booking_type_name`, `booking_from`, `date_created`, `is_active`) VALUES
(1, 'Private Chauffeur', 'service', '2015-04-17 16:00:00', 0),
(2, 'Package Deal', 'service', '2015-04-17 16:00:00', 0),
(3, 'Tour', 'tour', '2015-04-17 16:00:45', 0),
(4, 'Exclusive Tour', 'tour', '2015-04-17 16:00:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE IF NOT EXISTS `enquiries` (
  `enquiry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enquiry_type` varchar(150) NOT NULL,
  `enquiry_ref` varchar(50) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `enquiry_body` text NOT NULL,
  `enquiry_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enquiry_status` tinyint(1) NOT NULL DEFAULT '0',
  `enquiry_namtes` varchar(1500) NOT NULL,
  PRIMARY KEY (`enquiry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(150) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_types`
--

CREATE TABLE IF NOT EXISTS `service_types` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(50) NOT NULL,
  `service_content` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `parent` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`service_id`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `service_types`
--

INSERT INTO `service_types` (`service_id`, `service_name`, `service_content`, `date_created`, `is_active`, `parent`) VALUES
(1, 'Private Chauffeur', 'Chauffeur services offered is exclusive transfers from Cape Town international airport or point to point transfers, this exclusive service is available 24 hours a day, 7 days a week, 365 days of the year and it tailored to the individual’s and corporate traveler’s specific needs.\r\nWe offer a complimentary meet and greet service at the hotel or airport, adding a unique value of service and convenience for leisure and corporate passenger.\r\nOur team is full of trained professional chauffeurs who will ensure your safety and comfort whether you travelling to a sporting event, work function or to the airport. ', '2015-04-17 17:23:49', 0, 1),
(2, 'Airport + Hotel', 'Book our packages for your safe transportation for Airport and Hotel pickup and drop offs', '2015-04-17 17:25:17', 0, 2),
(3, 'Airport + Hotel + Tour', 'Book our packages for your safe transportation for Airport and Hotel pickup and drop offs as well as a tour', '2015-04-17 17:25:17', 0, 2),
(4, 'Hotel + Tour', 'Book our packages for your safe transportation for Hotel pickup and drop offs as well as a tour', '2015-04-17 17:25:17', 0, 2),
(5, 'Airport + Tour', 'Book our packages for your safe transportation for Airport pickup and drop offs as well as a tour', '2015-04-17 17:25:17', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_email` varchar(255) NOT NULL,
  `subscribe_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `subsctibe_value` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `subscriber_email_2` (`subscriber_email`),
  KEY `subscriber_email` (`subscriber_email`,`subsctibe_value`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subscriber_id`, `subscriber_email`, `subscribe_date`, `subsctibe_value`) VALUES
(2, 'deanvann@gmail.com', '2015-05-11 19:51:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `t_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `t_headline` varchar(400) NOT NULL,
  `t_author` varchar(255) NOT NULL,
  `t_body` text NOT NULL,
  `t_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `t_imge` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`t_id`, `t_headline`, `t_author`, `t_body`, `t_date`, `t_imge`) VALUES
(5, 'We had a wonderful time touring the Cape with you', 'Paul and Lesa', 'Hi Ismail\r\n\r\nIt was a real pleasure meeting you and starting what I hope will be a lasting friendship! I''ll send you photos when we get home and download the pictures.\r\n\r\nWe had a wonderful time touring the Cape with you. We felt safe, comfortable and happy to be in your radiant and knowledgeable company. I will enthusiastically recommend your services to all my friends who I hope will have the good fortune of going to Cape Town! Best wishes \r\n                                    ', '2013-03-15 22:00:00', ''),
(6, ' Thank you once again for taking good care of us!', 'Joerg & Ulrike - Germany', 'Hi Ismail, we still have nice holiday feelings in our minds, today we have met one of our best friends Dirk and his girlfriend Verena. They''ve booked SA end of Nov. I ''ve made a friend proposal on FB, u will meet them and showing them the most beautiful places in Cape town like u did for us.\r\n\r\nYou are not just gd and smart driver, you''re also fast connected, thank u once again for taking gd care of us!\r\n                                    ', '2013-09-01 22:00:00', 'camps_bay_12_apostles_-_1.jpg'),
(7, 'You''re the most responsible, entertaining lovely guide in Cape town', 'Moritz & Anke - Germany', 'Thank u so much Ismail, you''re the most responsible, entertaining lovely guide in Cape town.We''ll definitely recommend others, see u again. Regards\r\n                                    ', '2013-05-03 22:00:00', 'chaueffer services.JPG'),
(8, 'We will always use your service when we come to Cape town', 'Mr/Mrs Garber - Hawaii USA', 'Visited Cape town and we booked some nights at the Bay hotel, and we didn''t have \r\n\r\n\r\nanything in plan but a friend receptionist booked Ismail for us to show us around the Cape, and we had an amazing time with him, thank u for being so knowledgeable about everything from wine tours, Cape of good hope , townships etc can''t thank u enough indeed. We will always use your service when we come to Cape town. Kind regards\r\n                                    ', '2015-06-10 22:00:00', 'uploads-images-Media-gallery-Attractions TK-Constantiacellar.jpg'),
(9, 'Thank u and see u next time.', 'Torsten & Mel - Switzerland', 'Great experience with Ismail driving us around, safe, reliable always on time. He knows a lot about different places. Good fixed prices if u need him to wait until u finished your program.\r\n\r\nWe definitely will forward his number to all our friends who will need to visit Cape Town.\r\n\r\nThank u and see u next time.\r\n                                    ', '2013-04-17 22:00:00', 'uploads-images-Media-gallery-Nature TK-Boulders05 (1).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE IF NOT EXISTS `tours` (
  `tour_id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_name` varchar(255) NOT NULL,
  `tour_overview` text NOT NULL,
  `tour_about` text NOT NULL,
  `tour_map` varchar(100) DEFAULT NULL,
  `tour_tags` varchar(500) NOT NULL,
  `tour_main_image` varchar(100) NOT NULL,
  `tour_images` varchar(1500) NOT NULL,
  `is_special` tinyint(4) NOT NULL DEFAULT '0',
  `is_exclusive` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tour_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`tour_id`, `tour_name`, `tour_overview`, `tour_about`, `tour_map`, `tour_tags`, `tour_main_image`, `tour_images`, `is_special`, `is_exclusive`, `is_active`, `last_updated`) VALUES
(1, 'Cape+Peninsula', '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Join+us+as+we+travel+around+the+Cape+Peninsula%2C+named+by+early+explorers+as+the+%E2%80%98Fairest+Cape%E2%80%99.+On+this+tour+you+will+encounter+beautiful+beaches%2C+dramatic+scenery+%2C+the+mythical+meeting+place+of+the+two+oceans%2C+local+wildlife+and+world-famous+botanical+gardens.%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Highlights%0D%0A%0D%0ATravel+along+the+spectacular+coastline%0D%0AVisit+the+mystical+meeting+place+of+two+great+oceans%0D%0AClifton+Beaches%2C+Camps+Bay%0D%0AThe+Twelve+Apostles%0D%0ALlundudno%0D%0AHout+Bay%0D%0ASeal+Island+%28optional%29%0D%0AChapman%E2%80%99s+Peak+Drive%0D%0ANoordhoek%0D%0AOstrich+Viewing+%28optional%29%0D%0ACape+of+Good+Hope+-Nature+Reserve%0D%0ACape+Point+-South+Western+point+of+the+African+Continent%0D%0AFalse+Bay%0D%0APenguin+Colony%0D%0ASimon%E2%80%99s+Town%0D%0AMuizenburg%0D%0AKalk+Bay%0D%0AKirstenbosch+Botanical+Gardens+%28optional%29%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', 'map5.jpg', '', 'capepoint_1.jpg', 'ziwatours.png,engine mouting.jpg,ziwaologo.jpg', 0, 0, 1, '2015-06-09 16:22:24'),
(3, 'Township Tour', '                                        This tour shows how the majority of South Africans live in the buzzing townships. It is an excellent opportunity to interact with local communities and share in the rich African culture and traditional values. Experience unique township culinary delights, or try some home-made brew while enjoying some township jazz. Visit an African craft market, spaza shop, school, orphanage, community centre and a traditional healer.\r\n                                                                        ', '                                        Highlights:\r\n\r\nExperience how the majority live in the buzzing townships\r\nInteract with local communities\r\nShare the African culture and traditional values\r\nExperience unique township culinary delights\r\nTraditional Healer\r\nVisit an African craft market spaza shop, school, orphanage\r\nCommunity Housing\r\nTaste African beer in a Shebeen(optional), District six.\r\n                                                                        ', 'download.jpg', 'Full Day,Half Day,Packaged', 'town ship.jpg', '', 1, 0, 1, '2015-06-29 21:57:01'),
(4, 'Table Mountain', '                                                                                Towering 1088m over Cape town and around 500million years in the making, its a play ground for nature lovers and outdoor enthusiasts a like, trails runs the length and breath of the mountain offering exceptional hiking experience for all levels of fitness, get an unforgettable hiking experience with our fully qualified guides team\r\n                                                                                                            ', '                                                                                Highlights\r\n\r\nCity Centre,\r\nBo-Kaap, Table-Mountain cable car (weather permitting),\r\nVisit Signal Hill\r\n                                                                                                            ', 'download (1).jpg', 'Full Day,Half Day,Packaged', 'table mountain.jpg', '', 1, 0, 1, '2015-06-29 21:54:19'),
(5, 'Robben Island', '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Robben+Island+%28Afrikaans%3A+Robbeneiland%29+is+an+island+in+Table+Bay%2C+6.9+km+west+of+the+coast+of+Bloubergstrand%2C+Cape+Town%2C+South+Africa.+The+name+is+Dutch+for+%22seal+island.%22+Robben+Island+is+roughly+oval+in+shape%2C+3.3+km+long+north-south%2C+and+1.9+km+wide%2C+with+an+area+of+5.07+km%C2%B2.%5B2%5D+It+is+flat+and+only+a+few+metres+above+sea+level%2C+as+a+result+of+an+ancient+erosion+event.+The+island+is+composed+of+Precambrian+metamorphic+rocks+belonging+to+the+Malmesbury+Group.%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Highlights%0D%0A%0D%0AReturn+Fairy+trip%2Ftour+of+the+political+prison.%0D%0ATour+of+the+former+political+prison+by+an+ex-inmate%0D%0ATour+of+the+limestone+quarry%0D%0ASee+the+prison+cell+that+held+Nelson+Mandela+captive+for+++so+many+years.%0D%0AV+%26+A+Waterfront%0D%0ASeal+Island+Boat+rides+%28weather+permitting%29%0D%0A%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', 'download (2).jpg', 'Full Day,Half Day,Packaged', 'Robben island.jpg', '', 1, 0, 1, '2015-06-29 21:51:50'),
(7, 'Cape Winelands', 'The world-famous Cape wine farms are a visual feast of lush green valleys, rugged mountains and fine cuisine. The spectacular scenery forms a backdrop to the unique Cape Dutch architecture, with its distinctive gables and thatched roofs.\r\n                                    ', 'Option 1: Franschhoek\r\n\r\nExplore a Franschoek Vineyard\r\nHelshoogte Pass\r\nHistorical Village\r\nHuguenot Memorial\r\n\r\n\r\nOption 2: Stellenbosch\r\n\r\nCity Tour  – free walkabout in this historical University Town\r\nExplore a Stellenbosch Estate\r\nCellar tour\r\nDorp Street\r\nStellenbosch village houses\r\n\r\n\r\nOption 3: Paarl\r\n\r\nDrive through Paarl\r\nExplore a Paarl    Estate\r\nView Nelson Mandela’s former prison (Victor Vester)\r\n\r\n\r\nOptional:\r\n\r\nStrawberry Picking\r\nPicnic on the farm\r\nCheese Tasting\r\nCellar Tour\r\nOpportunity to shop & export goods overseas\r\n                                    ', 'Rural-Wine-250b.jpg', 'Full Day,Half Day,Garden Route,Extended,Packaged', 'Cape winelands.jpg', '', 1, 0, 1, '2015-06-29 22:02:20'),
(8, 'Whale Route', 'The beautiful town of Hermanus just outside Cape Town, lies along the shores of Walker Bay near the southernmost tip of Africa. Magnificent mountains watch over the town, which is home to the Southern Right Whales which frequent our shores annually from June to November. Nature lovers from all over the world visit Hermanus to view these magnificent creatures from the best land-based whale watching sight in the world. Listen out for the world famous whale crier’s horn which is sounded upon sightings in the bay. This little town is perfect for spending a couple days in to soak up the atmosphere. Just further is the town of Gaanbaai where one can go Shark Cage Diving.\r\n\r\n\r\nHermanus lies along the shores of Walker Bay near the southern most tip of Africa. Magnificent mountains watch over the town, which is home to the Southern Right Whales which frequent our shores annually from June to November. Nature lovers from all over the world visit Hermanus to view these magnificent creatures from the best land-based whale watching sight in the world.\r\n                                                                        ', '                                        Highlights\r\n\r\nBoat trip Whale Watching (optional)\r\nListen out for the whale crier’s horn\r\nDrive through magnificent mountains\r\nFree walkabout in the beautiful town of Hermanus\r\nHarold Porter Botanical Gardens (optional)\r\nPringle Bay\r\nBetty’s Bay(penguin viewing optional)\r\nKleinmond\r\nSir Lowry’s Pass\r\nDrive through the Wine Estate (Except Sundays)\r\nWine Tasting at one of the top wine farms (optional)\r\nView Seals, African penguins, Marine birds, dolphins and\r\nSouthern Right Whales as well as the Humpback whales\r\n                                                                        ', 'download.jpg', 'Full Day,Extended,Packaged', 'overberg_2.jpg', '', 1, 1, 1, '2015-05-18 17:48:03'),
(9, 'Shark Cage Diving', '                                                                                Experience cage diving and boat viewing in the False Bay coast. No diving experience necessary.\r\n                                                                                                            ', '                                                                                Highlights\r\n\r\nEarly departure with door-to-door pickup and drop-off\r\n\r\nBoat trip to dive site\r\n\r\nDive equipment (dive certificate is not compulsory)\r\n\r\nLight breakfast with morning briefing\r\nLunch and drinks on board\r\n\r\nThe cage is reinforced galvanised steel of the highest quality, which is lowered next to the boat and floats along side it. It’s a 6 man cage so the clients will rotate the cage viewing, however there is great viewing from up on board. Two highly skilled Dive Masters and a level 3 medic with oxygen management, supervise all dives. The boats and operations are monitored by SAMSA (South African Maritime Safety Association), you get a chance to encounter Southern Right whales especially during spring season, pass through or make a quick stop over in historical town of Hermanus.\r\n                                                                                                            ', '360px-False_Bay_offshore_dive_sites.png', 'Full Day,Packaged', 'leave that one  on shark diving and delete the one in the cage.jpg', 'shark.jpg', 1, 1, 1, '2015-06-29 21:58:45'),
(10, 'Safari', '                                        Just 2 hours from Cape Town, you can now experience for the first time in over a century the return of Africa’s largest wild animals to this region. Set in a big green oasis in the little karoo, delight yourself with the sights and sounds of a wide variety of game.\r\n\r\n\r\n                                                                        ', '                                        Highlights\r\n\r\nExperience a true African Safari – Big  5\r\nThe home to Lions as well as free roaming Elephants,    Buffalos and Rhinos\r\nBig variety of game such as black Wildebeest,    Zebra,  Springbok, \r\nGemsbok , Steenbok , Blesbok , Ostriches, Klipspringer, Duiker, Dassie, Baboons, Eland, red Har tebeest, large amount of birds species and many more\r\nEnjoy the mountains surrounded by Cape  Fynbos  and water falls\r\nWelcoming drinks and a light breakfast before departing on a 2–3 hour game drive\r\n                                                                        ', 'download (2).jpg', 'Full Day,Packaged', 'big five.jpg', '', 1, 1, 1, '2015-06-29 21:55:32'),
(11, 'West Coast/Flower Tour.', '                                                                                                                        The magician behind this seemingly sudden change is the spring rain, which allows dormant seeds to germinate and erupt onto the surface, blossoming in phenomenal brilliance. Yellow, pink, orange and purple carpets of colour spread as far as the eye can see. The flower season is rain-dependent so the spring blooming normally occurs between August and October\r\n                                                                                                                                                ', '                                                                                                                        Highlight:\r\n\r\nVisit the spring bloom with flower-gazers marvel at a dramatic botanical phenomenon\r\nEnjoy a dry, brown and seemingly barren landscape metamorphoses into a breathtaking floral wonderland\r\nExperience blossoming in phenomenal brilliance\r\nYellow, pink, orange and purple carpets of colour spread as far as the eye can see\r\nAreas we visit are Melkbosstrand, West coast National Park, Darling\r\nHorse riding (optional)\r\nOstrich viewing (optional ), Langebaan.\r\n                                                                                                                                                ', 'download (3).jpg', 'Full Day,Half Day', 'west coast.JPG', '', 1, 1, 1, '2015-06-29 22:00:51'),
(12, 'Jewellery .', '                                                                                Visit a leading Jewellery and Diamond Manufacturer situated at the corner of Loop and Hout Street, Cape Town. You and your guests are invited to a remarkable Diamond and Tanzanite experience at the historic Huguenot House.\r\n                                                                                                            ', '                                                                                Highlights\r\n\r\nMaster Goldsmiths\r\nAward winning design\r\n4 Graduate Gemmologists\r\nHistorical and factory tours\r\nFully certified jewellery\r\n                                                                                                            ', '', 'Half Day,Packaged', 'images (2).jpg', '', 0, 0, 1, '2015-06-09 08:05:13'),
(13, 'Kids+Recreational+Activities', '++++++++++++++++++++++++++++++++++++++++We+have+a+range+of+activities+below+from+some+of+the+best+children%E2%80%99s+activities+to+do+in+and+around+Cape%3Cbr%3ETown+and+let+our+guide+take+them+on+a+fun+day+out.++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', '++++++++++++++++++++++++++++++++++++++++Pick+2+or+3+activities+from+below+and+we+will+take+care+of+the+rest%0D%0AJoin+your+child+or+book+a+Guide+to+accompany+them%0D%0AWe+book+and+arrange+your+Transport%0D%0A%0D%0AKenilworth+Karting%0D%0AFor+the+ultimate+adrenaline+rush%2C+race+a+160cc+kart+around+a+high-tech%2C+300m+indoor%0D%0Atrack.+Underage+drivers+can+tear+up+the+tracks+in+140cc+junior+carts.%0D%0A%0D%0ALets+Go+Bowling+Stadium+Bowl%0D%0ATen+pin+bowling+is+an+affordable+and+social+game+that+can+be+enjoyed+by+all+ages.%0D%0ALet+%E2%80%99s+Go+Bowling+cater+for+children%E2%80%99s+birthday+parties.%0D%0A%0D%0ATwo+Oceans+Aquarium%0D%0AThe+aquarium+showcases+over+3000+living+sea+animals+including%3B+sharks%2C+fishes%2C%0D%0Aturtles+and+penguins.%0D%0A%0D%0ARatanga+Junction%0D%0AOne+of+the+most+nearest+kids+funny+parks+from+Cape+town+CBD%2C+right+opposite++Canal+walk+the+biggest+shopping+mall+in+the+southern+hemisphere.++++++++++++++++++++++++++++++++++++', '', 'Full Day,Half Day', 'kids recreational activities.jpg', '', 1, 0, 1, '2015-06-29 21:40:13'),
(14, 'Garden Route', '                                        The Garden Route region runs along a scenic stretch of coastline beginning in CapeTown, travelling through Mossel Bay, George, Wilderness, Knysna, Plettenberg Bay, Tsitsikamma and ending in Port Elizabeth.The region has become South Africa’s most popular tourist destination after Cape Town.Visitors are drawn year-round to its indigenous forests, freshwater lakes,wetlands, hidden coves and long beaches.We will tailor-make your Garden Route tour, which will usually last 3 to 4 days, and may include the following…\r\n                                                                        ', '                                        Highlights:\r\n\r\nRegion runs along a scenic stretch of coastline travelling through Mossel Bay, George, Wilderness, Knysna, Plettenberg Bay and Oudtshoorn\r\nSouth Africa’s most popular tourist destination after Cape Town\r\nSee Indigenous forests, freshwater lakes, wetlands, hidden coves and beaches\r\nCango Caves\r\nWildlife Ranch\r\nOuteniqua Choo Choo\r\nKnysna and the Knysna Heads\r\nFeatherbed Nature Reserve\r\nGame Reserve\r\nAddo Elephant Park\r\nBird of Eden\r\nMonkey land\r\nTsitsikamma\r\nWe can tailor make your Garden Route Tour\r\n                                                                        ', 'download (4).jpg', 'Full Day,Packaged', 'garden route.jpg', '', 1, 0, 1, '2015-06-29 21:49:16'),
(15, 'Big+6', '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Cape+Town+and+the+Big+6%0D%0A%0D%0AWhat+is+the+Big+Six%3F+-+It+is+Six+unforgettable+Experiences+in+One+Destination.%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Highlights%0D%0A%0D%0ATable+Mountain+Cableway%0D%0AV+%26+A+Waterfront%0D%0ARobben+Island%0D%0AKirstenbosch%0D%0AConstantia+Vineyards%0D%0ACape+Point%0D%0A%0D%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', 'images (1).jpg', 'Full Day,Packaged', 'Big 6.JPG', '', 1, 0, 1, '2015-06-29 21:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `tour_type`
--

CREATE TABLE IF NOT EXISTS `tour_type` (
  `tour_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tour_type_name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tour_type_id`),
  KEY `tour_type_id` (`tour_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_last_name` varchar(45) NOT NULL,
  `user_cell` varchar(14) NOT NULL,
  `user_gender` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 is male 2 is female',
  `user_country` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_pass` varchar(45) NOT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '0',
  `user_role` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `date_created` (`date_created`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_last_name`, `user_cell`, `user_gender`, `user_country`, `user_email`, `user_pass`, `user_status`, `user_role`, `date_created`) VALUES
(1, 'Ismail', 'Ziwa', '0815575271', 1, 'Uganda', 'info@ziwatours.biz', '7488e331b8b64e5794da3fa4eb10ad5d', 0, 2, '2015-04-17 16:18:52'),
(2, 'Dean', 'Smith', '1234567', 1, 'USA', 'deanvann@gmail.com', '7488e331b8b64e5794da3fa4eb10ad5d', 0, 1, '2015-04-17 16:18:52'),
(3, 'Grant', '', '', 1, '', 'jcicapetown2015@gmail.com', 'd5f8e37a0c4f58fd305f73a460d20b40', 0, 1, '2015-05-21 17:01:31'),
(4, 'Zabrave', '', '', 1, '', 'Zabrave7@gmail.com', '9801e921a09f65473a540c92e2437887', 0, 1, '2015-06-29 22:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `ziwa_grouped_stats`
--

CREATE TABLE IF NOT EXISTS `ziwa_grouped_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `private` int(10) unsigned NOT NULL,
  `packages` int(10) unsigned NOT NULL,
  `tours` int(10) unsigned NOT NULL,
  `exclusives` int(10) unsigned NOT NULL,
  `stat_month` tinyint(2) NOT NULL,
  `stat_year` int(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ziwa_grouped_stats`
--

INSERT INTO `ziwa_grouped_stats` (`id`, `private`, `packages`, `tours`, `exclusives`, `stat_month`, `stat_year`, `last_updated`) VALUES
(2, 3, 0, 2, 1, 5, 2015, '2015-05-21 17:01:32'),
(3, 0, 0, 2, 0, 6, 2015, '2015-06-29 22:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `ziwa_overall_stats`
--

CREATE TABLE IF NOT EXISTS `ziwa_overall_stats` (
  `private` int(10) unsigned NOT NULL,
  `packages` int(10) unsigned NOT NULL,
  `tours` int(10) unsigned NOT NULL,
  `exclusive` int(10) unsigned NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ziwa_overall_stats`
--

INSERT INTO `ziwa_overall_stats` (`private`, `packages`, `tours`, `exclusive`, `last_updated`) VALUES
(3, 0, 4, 3, '2015-06-29 22:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `ziwa_updates`
--

CREATE TABLE IF NOT EXISTS `ziwa_updates` (
  `update_id` int(9) NOT NULL AUTO_INCREMENT,
  `update_from` varchar(50) NOT NULL,
  `update_to` varchar(50) NOT NULL,
  `update_type` tinyint(1) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_content` text NOT NULL,
  `update_subject` varchar(200) NOT NULL,
  `update_parent` int(9) NOT NULL,
  `update_read` tinyint(1) NOT NULL DEFAULT '0',
  `update_status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_id`),
  KEY `update_from` (`update_from`,`update_to`,`update_type`,`update_date`,`update_parent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `ziwa_updates`
--

INSERT INTO `ziwa_updates` (`update_id`, `update_from`, `update_to`, `update_type`, `update_date`, `update_content`, `update_subject`, `update_parent`, `update_read`, `update_status`) VALUES
(1, 'deanvann@gmail.com', 'info@ziwatours.biz', 1, '2015-04-19 18:23:56', 'I would like to know more about the tours you offer', 'Tour Enquiry', 0, 1, 0),
(2, 'deanvann@gmail.com', 'info@ziwatours.biz', 1, '2015-04-19 18:25:31', 'How can i get the latest specials on private drivers ', 'Other', 0, 0, 0),
(11, 'deanvann@gmail.com', 'info@ziwatours.biz', 1, '2015-04-22 07:05:39', 'I love this tour can you please let me know if there are any specials i can apply for', 'Tour Enquiry', 0, 1, 0),
(12, 'deanvann@gmail.com', 'info@ziwatours.biz', 1, '2015-04-22 07:05:50', 'some more enquiries', 'General Enquiry', 0, 0, 0),
(13, 'info@ziwatours.biz', 'deanvann@gmail.com', 2, '2015-04-22 07:08:28', 'this is a message from your admin', 'General Notification', 0, 0, 0),
(14, 'info@ziwatours.biz', 'deanvann@gmail.com', 2, '2015-04-22 07:08:28', 'a verimportant announcement', 'General Announcement', 0, 1, 0),
(15, 'info@ziwatours.biz', 'deanvann@gmail.com', 1, '2015-05-02 18:37:13', 'great to hear that it works out for you if you need anything else please let me know \r\n                                    ', 'General Enquiry', 12, 0, 0),
(17, 'info@ziwatours.biz', 'deanvann@gmail.com', 1, '2015-05-13 20:23:13', 'Your Booking details have been updated to view details click this link : <a href=''/members/view_booking/''.4.''_''.BSSSoXBq1DULIWaSCSrL''>Updated Booking Details</a>', 'Booking Update', 0, 1, 0),
(18, 'info@ziwatours.biz', 'deanvann@gmail.com', 1, '2015-05-13 20:30:43', 'Your Booking details have been updated to view details click this link : <a href="/members/view_booking/4_BSSSoXBq1DULIWaSCSrL">Updated Booking Details</a>', 'Booking Update', 0, 0, 0),
(19, 'info@ziwatours.biz', 'deanvann@gmail.com', 1, '2015-05-13 20:31:02', 'Your Booking details have been updated to view details click this link : <a href="/members/view_booking/4_BSSSoXBq1DULIWaSCSrL">Updated Booking Details</a>', 'Booking Update', 0, 1, 0),
(20, 'info@ziwatours.biz', 'deanvann@gmail.com', 1, '2015-05-13 20:31:27', 'Your Booking details have been updated to view details click this link : <a href="/members/view_booking/4_BSSSoXBq1DULIWaSCSrL">Updated Booking Details</a>', 'Booking Update', 0, 0, 0),
(21, 'info@ziwatours.biz', 'deanvann@gmail.com', 1, '2015-05-13 20:35:26', 'Your Booking details have been updated to view details click this link : <a href="/members/view_booking/4_BSSSoXBq1DULIWaSCSrL">Updated Booking Details</a>', 'Booking Update', 0, 0, 0),
(22, 'jcicapetown2015@gmail.com', 'info@ziwatours.biz', 1, '2015-05-23 18:34:24', 'hi team, \r\n\r\nI have not yet received any feedback regarding my tour.\r\n\r\nthanks ', 'General Enquiry', 0, 1, 0),
(23, 'Zabrave7@gmail.com', 'info@ziwatours.biz', 1, '2015-06-29 22:29:47', 'Hi ismail, \r\nI contacted you couples of days ago for a tour. still waiting \r\n\r\nThanks ', 'General Enquiry', 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
