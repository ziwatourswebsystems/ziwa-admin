<?php

class services_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function get_exclusive_services()
    {
        $this->db->from("tours");
        $this->db->where('is_exclusive', 1);
        $this->db->order_by("tour_id", "DESC");
        $query = $this->db->get();
        return $query->result_array();

    }

    public function get_tour_services()
    {
        $this->db->from("tours");
        $this->db->where('is_exclusive', 0);
        $this->db->order_by("tour_id", "DESC");
        $query = $this->db->get();
        return $query->result_array();

    }

    public function get_package_services()
    {
        $this->db->from("service_types");
        $this->db->where('parent', 2);
        $this->db->order_by("service_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();

    }

    public function get_private_services()
    {
        $this->db->from("service_types");
        $this->db->where('parent', 1);
        $this->db->order_by("service_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();

    }

    public function get_all_services()
    {
        $this->db->from("service_types");
        $this->db->order_by("service_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

   public function add_service($service_name,$service_body)
   {
       $data = array(
           'service_name' => $service_name,
           'service_content' => $service_body
       );

       $this->db->insert('service_types', $data);
   }

    public function update_service_status($service_id,$service_value)
    {
        $data = array(
            'is_active' => $service_value
        );

        $this->db->where('service_id', $service_id);
        $this->db->update('service_types', $data);
    }

    public function delete_service($s_id)
    {
        $this->db->where('service_id', $s_id);
        $this->db->delete('service_types');
    }
}