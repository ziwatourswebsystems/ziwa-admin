<?php

class message_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function get_all_messages()
    {
        $this->db->from("ziwa_updates");
        $this->db->where('update_to', "info@ziwatours.biz");
        $this->db->order_by("update_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function get_message_details($message_parent_id)
    {
        $this->db->from("ziwa_updates");
        $this->db->where('update_id', $message_parent_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row();
    }

    public function add_message($parent,$subject,$to,$body)
    {
        $data = array(
            'update_from' => 'info@ziwatours.biz' ,
            'update_to' => $to,
            'update_type' => "1",
            'update_content' => $body,
            'update_subject' => $subject,
            'update_parent' => $parent
        );

        $this->db->insert('ziwa_updates', $data);
    }
}