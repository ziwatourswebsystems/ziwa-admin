<?php

class tours_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }


    public function get_all_tours()
    {
        $this->db->from("tours");
        $this->db->order_by("tour_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function update_tour($tour_id,$tour_name,$tour_overview,$tour_about,$is_special,$is_exclusive,$tour_tags)
    {
        $data = array(
            'tour_name' => $tour_name,
            'tour_overview' => $tour_overview,
            'tour_about' => $tour_about,
            'is_special' => $is_special,
            'is_exclusive' => $is_exclusive,
            'tour_tags' => $tour_tags
        );

        $this->db->where('tour_id', $tour_id);
        $this->db->update('tours', $data);
    }

    public function get_tour_detail($t_id)
    {
        $this->db->from("tours");
        $this->db->where('tour_id', $t_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row();
    }

    public function add_tour($tour_name,$tour_overview,$tour_about,$is_special,$is_exclusive,$tour_tags)
   {
       $data = array(
           'tour_name' => $tour_name,
           'tour_overview' => $tour_overview,
           'tour_about' => $tour_about,
           'is_special' => $is_special,
           'is_exclusive' => $is_exclusive,
           'tour_tags' => $tour_tags
       );

       $this->db->insert('tours', $data);
   }

    public function update_tour_map($t_id,$map_image)
    {
        $data = array(
            'tour_map' => $map_image
        );

        $this->db->where('tour_id', $t_id);
        $this->db->update('tours', $data);
    }

    public function update_tour_main_image($t_id,$main_image)
    {
        $data = array(
            'tour_main_image' => $main_image
        );

        $this->db->where('tour_id', $t_id);
        $this->db->update('tours', $data);
    }

    public function update_tour_images($t_id,$tour_images)
    {
        $this->db->select('tour_images');
        $this->db->from('tours');
        $this->db->where('tour_id', $t_id);
        $current_images = $this->db->get()->row()->tour_images;
        if(strlen($current_images) > 2) {
            $current_images = $current_images . "," . $tour_images;
        }else{
            $current_images = $tour_images;
        }

        $this->db->set('tour_images', $current_images);
        $this->db->where('tour_id', $t_id);
        $this->db->update('tours');
    }

    public function update_tour_special($t_id,$value)
    {
        $data = array(
            'is_special' => $value
        );

        $this->db->where('tour_id', $t_id);
        $this->db->update('tours', $data);
    }

    public function update_tour_exclusive($t_id,$value)
    {
        $data = array(
            'is_exclusive' => $value
        );

        $this->db->where('tour_id', $t_id);
        $this->db->update('tours', $data);
    }

    public function delete_tour($t_id)
    {
        $this->db->where('tour_id', $t_id);
        $this->db->delete('tours');
    }

    public function send_tour_email($email,$user_name,$tour_details)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Welcome to Ziwa Tours and Thank you for choosing us as your travel guide. You are now an official member of Ziwa Tours</p>";
        $message .= "<p>Please check out your member dashboard where you can create, update booking as well as recieve exclusive specials and oofers from Ziwa Tours.</p>";
        $message .= "<p>You Login Details as username :".$email." password : ".$user_pass." to access your membership dashboard you can go to http://members.ziwatours.biz/</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Welcome to Ziwa Tours');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}