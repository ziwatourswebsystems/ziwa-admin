<?php

class Auth_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function updateUserProfile($user_id,$fname,$sname,$celln,$gender,$country)
    {
        $data = array(
            'user_name' => $fname,
            'user_last_name' => $sname,
            'user_cell' => $celln,
            'user_gender' => $gender,
            'user_country' => $country
        );

        $this->db->where('user_email', $user_id);
        $this->db->update('users', $data);

    }

    public function updateUserPassword($email,$password)
    {
        $data = array(
            'user_pass' => md5($password)
        );

        $this->db->where('user_email', $email);
        $this->db->update('users', $data);
    }

	//Validates if user exists
    public function check_user($email,$user_name)
    {
        $this->db->where('user_email',$email );
        $query = $this->db->get('users');
        if($query->num_rows() == 0)
        {
            $this->create_user($email,$user_name);
            return $this->check_user($email,$user_name);

        }
        return $query->row_array();
    }

    public function check_user_creds($email,$password)
    {
        $this->db->where('user_email',$email);
        $this->db->where('user_pass',md5($password));
        $query = $this->db->get('users');
        //echo $this->db->last_query();

        if($query->num_rows() == 0)
        {
            return false;
        }
        else
        {
            return $query->row_array();
        }
    }


    public function SendZiwa_Message($from,$to,$message_type,$subject,$message_body,$message_parent)
    {
        $data = array(
            'update_from' => $from,
            'update_to' => $to,
            'update_type' => $message_type,
            'update_content' => $message_body,
            'update_subject' => $subject,
            'update_parent' => $message_parent
        );
        $this->db->insert('ziwa_updates', $data);
    }


    public function create_user($email,$user_name)
    {
        $raw_password = random_string('alnum',8);
        $data = array(
            'user_name' => $user_name,
            'user_email' => $email,
            'user_pass' => md5($raw_password)
        );
        $this->db->insert('users', $data);
        //$this->send_welcome_email($email,$user_name,$raw_password);

    }

    public function send_welcome_email($email,$user_name,$user_pass)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Welcome to Ziwa Tours and Thank you for choosing us as your travel guide. You are now an official member of Ziwa Tours</p>";
        $message .= "<p>Please check out your member dashboard where you can create, update booking as well as recieve exclusive specials and oofers from Ziwa Tours.</p>";
        $message .= "<p>You Login Details as username :".$email." password : ".$user_pass." to access your membership dashboard you can go to http://members.ziwatours.biz/</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Welcome to Ziwa Tours');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}