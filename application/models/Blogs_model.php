<?php

class blogs_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function get_all_blogs()
    {
        $this->db->from("blog");
        $this->db->order_by("blog_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

   public function add_blog($name,$body,$summary,$blog_tags)
   {
       $data = array(
           'blog_title' => $name,
           'blog_body' => $body,
           'blog_summary' => $summary,
           'blog_tags' => $blog_tags
       );

       $this->db->insert('blog', $data);
   }


    public function update_blog_main_image($t_id,$main_image)
    {
        $data = array(
            'blog_image' => $main_image
        );

        $this->db->where('blog_id', $t_id);
        $this->db->update('blog', $data);
    }

    public function delete_blog($t_id)
    {
        $this->db->where('blog_id', $t_id);
        $this->db->delete('blog');
    }

}