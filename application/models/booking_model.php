<?php

class booking_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    //Validates if user exists
    public function increment_stats($stat_type_id)
    {
        echo "type : ".$stat_type_id;
        // increment global figures
        if($stat_type_id == 1) {
            $this->db->set("private", "private + 1", FALSE);
        }
        elseif($stat_type_id == 2) {
            $this->db->set("packages", "packages + 1", FALSE);
        }
        elseif($stat_type_id == 3) {
            $this->db->set("tours", "tours + 1", FALSE);
        }
        elseif($stat_type_id == 4) {
            $this->db->set("exclusive", "exclusive + 1", FALSE);
        }
        $this->db->update('ziwa_overall_stats');

        $curYear = date('Y');
        $curMonth = date('m');

        $this->db->where('stat_month',$curMonth);
        $this->db->where('stat_year',$curYear);
        $q = $this->db->get('ziwa_grouped_stats');

        if ( $q->num_rows() == 0 )
        {
            $data = array(
                'stat_month' => $curMonth,
                'stat_year' => $curYear
            );
            $this->db->insert('ziwa_grouped_stats', $data);
        }

        // increment monthly figures
        if($stat_type_id == 1) {
            $this->db->set("private", "private + 1", FALSE);
        }
        elseif($stat_type_id == 2) {
            $this->db->set("packages", "packages + 1", FALSE);
        }
        elseif($stat_type_id == 3) {
            $this->db->set("tours", "tours + 1", FALSE);
        }
        elseif($stat_type_id == 4) {
            $this->db->set("exclusives", "exclusives + 1", FALSE);
        }
        $this->db->where('stat_month', $curMonth);
        $this->db->where('stat_year', $curYear);
        $this->db->update('ziwa_grouped_stats');

    }

    public function add_Booking($user_id,$booking_date,$booking_type_id,$booking_id,$booking_adults,$booking_children)
    {
        $transaction_tokem = random_string('alnum',20);
        $fdate=date('Y-m-d',strtotime($booking_date));
        $data = array(
            'user_id' => $user_id,
            'booking_type_id' => $booking_type_id,
            'booking_id' => $booking_id,
            'adults' => $booking_adults,
            'children' => $booking_children,
            'date_booked_for' => $fdate,
            'status' => 0,
            'transaction_token' => $transaction_tokem
        );

        $this->db->insert('booking_transaction', $data);

        $transaction_id = $id = $this->db->insert_id();
        $transaction_hash = $transaction_id."_".$transaction_tokem;
        $url = "http://members.ziwatours.biz/members/view_booking/".$transaction_hash;
        $this->increment_stats($booking_type_id);
        $this->send_booking_reservation_email($user_id,$url);
    }

    public function accept_booking_admin($booking_id)
    {
        $data = array(
            'status' => 3
        );

        $this->db->where('booking_transaction_id', $booking_id);
        $this->db->update('booking_transaction', $data);
    }


    public function cancel_booking_admin($booking_id)
    {
        $data = array(
            'status' => 4
        );

        $this->db->where('booking_transaction_id', $booking_id);
        $this->db->update('booking_transaction', $data);
    }

    public function accept_booking($booking_id)
    {
        $data = array(
            'status' => 3
        );

        $this->db->where('booking_transaction_id', $booking_id);
        $this->db->update('booking_transaction', $data);
    }


    public function cancel_booking($booking_id)
    {
        $data = array(
            'status' => 4
        );

        $this->db->where('booking_transaction_id', $booking_id);
        $this->db->update('booking_transaction', $data);
    }

    public function update_message_read_status($message_id)
    {
        $data = array(
            'update_read' => 1
        );

        $this->db->where('update_id', $message_id);
        $this->db->update('ziwa_updates', $data);
    }

    public function get_message_details($message_id)
    {
        $this->db->from("ziwa_updates");
        $this->db->where('update_id', $message_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row();
    }

    public function get_all_user_updates($user_id)
    {
        $this->db->from("ziwa_updates");
        $this->db->where('update_from', $user_id);
        $this->db->or_where('update_to', $user_id);
        $this->db->or_where('update_to', "all");
        $this->db->where('update_status', 0);
        $this->db->order_by("update_read", "ASC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function get_user_unread_updates_from($user_id)
    {
        $this->db->select("update_type as type_id, count(update_type) as type_count");
        $this->db->from("ziwa_updates");
        //$this->db->where('update_from', $user_id);
        $this->db->where('update_read', "0");
        $this->db->where('update_status', "0");
        //$this->db->where('update_to', $user_id);
        //$this->db->or_where('update_to', "all");
        $this->db->where('update_from', $user_id);
        $this->db->group_by("update_type");
        $this->db->order_by("update_read", "ASC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }


    public function get_user_unread_updates_to($user_id)
    {
        $this->db->select("update_type as type_id, count(update_type) as type_count");
        $this->db->from("ziwa_updates");
        //$this->db->where('update_from', $user_id);
        $this->db->where('update_read', "0");
        $this->db->where('update_status', "0");
        $this->db->where('update_to', $user_id);
        $this->db->or_where('update_to', "all");
        //$this->db->where('update_from', $user_id);
        $this->db->group_by("update_type");
        $this->db->order_by("update_read", "ASC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function get_book_name($booking_id,$from)
    {
        if($from == "service"){
            $this->db->select("service_name as bname");
            $this->db->from("service_types");
            $this->db->where('service_id', $booking_id);
        }else{
            $this->db->select("tour_name as bname");
            $this->db->from("tours");
            $this->db->where('tour_id', $booking_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row();

    }

    public function get_book_type($btypeid)
    {
        $this->db->from("booking_types");
        $this->db->where('booking_type_id', $btypeid);
        $query = $this->db->get();
        return $query->row();
    }

    public function update_booking_details($booking_id,$adults,$children,$book_date,$book_status )
    {
        $data = array(
            'adults' => $adults,
            'children' => $children,
            'date_booked_for' => $book_date,
            'status' => $book_status
        );

        $this->db->where('booking_transaction_id', $booking_id);
        $this->db->update('booking_transaction', $data);
    }

    public function get_booking_user($bkey)
    {
        $this->db->from("booking_transaction");
        $this->db->where('booking_transaction_id', $bkey);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_booking_info($bkey,$btoken)
    {
        $this->db->from("booking_transaction");
        $this->db->where('booking_transaction_id', $bkey);
        $this->db->where('transaction_token', $btoken);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_latest_updates($email)
    {
        $this->db->from("ziwa_updates");
        $this->db->where('update_from', $email);
        $this->db->or_where('update_to', $email);
        $this->db->or_where('update_to', "all");
        $this->db->order_by("update_date", "ASC");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_latest_booking($email)
    {
        $this->db->select("booking_types.booking_type_name, booking_transaction.status,booking_transaction.booking_transaction_id,booking_transaction.transaction_token,booking_transaction.date_booked_for");
        $this->db->from("booking_transaction");
        $this->db->where('user_id', $email);
        $this->db->where('status < ', '5');
        $this->db->join('booking_types', 'booking_types.booking_type_id = booking_transaction.booking_type_id', 'left');
        $this->db->order_by("date_booked_for", "ASC");
        $query = $this->db->get();
        return $query->result_array();

    }

    public function get_latest_booking_all()
    {
        $this->db->select("booking_types.booking_type_name, booking_transaction.status,booking_transaction.booking_transaction_id,booking_transaction.transaction_token,booking_transaction.date_booked_for");
        $this->db->from("booking_transaction");
        $this->db->where('status < ', '5');
        $this->db->join('booking_types', 'booking_types.booking_type_id = booking_transaction.booking_type_id', 'left');
        $this->db->order_by("date_booked_for", "ASC");
        $query = $this->db->get();
        return $query->result_array();

    }

    public function get_live_totals()
    {

        $this->db->select('count(*) as tot, DATE_FORMAT(date_booked_for, "%M %Y") as myformat')->from('booking_transaction');
        $this->db->group_by('MONTH(date_booked_for), YEAR(date_booked_for)');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_count_enquiries()
    {
        $this->db->select('*')->from('ziwa_updates');
        $q = $this->db->get();
        return $q->num_rows();
    }

    public function get_booking_stats_all()
    {
        $this->db->select("booking_types.booking_type_name, count(booking_transaction.booking_type_id) as type_count");
        $this->db->from("booking_transaction");
        $this->db->join('booking_types', 'booking_types.booking_type_id = booking_transaction.booking_type_id', 'left');
        $this->db->group_by("booking_transaction.booking_type_id");
        $this->db->order_by("booking_transaction.booking_type_id", "ASC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function get_booking_stats($email)
    {
        $this->db->select("booking_types.booking_type_name, count(booking_transaction.booking_type_id) as type_count");
        $this->db->from("booking_transaction");
        $this->db->where('booking_transaction.user_id', $email);
        $this->db->join('booking_types', 'booking_types.booking_type_id = booking_transaction.booking_type_id', 'left');
        $this->db->group_by("booking_transaction.booking_type_id");
        $this->db->order_by("booking_transaction.booking_type_id", "ASC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function send_welcome_email($email,$user_name,$user_pass)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Welcome to Ziwa Tours and Thank you for choosing us as your travel guide. You are now an official member of Ziwa Tours</p>";
        $message .= "<p>Please check out your member dashboard where you can create, update booking as well as recieve exclusive specials and oofers from Ziwa Tours.</p>";
        $message .= "<p>You Login Details as username :".$email." password : ".$user_pass." to access your membership dashboard you can go to http://members.ziwatours.biz/</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Welcome to Ziwa Tours');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

    public function send_booking_reservation_email($email,$url)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$email."</p>";
        $message .= "<p>Thank you for choosing us as your travel guide. Your booking has been provisionally placed.</p>";
        $message .= "<p>Please check your member dashboard for full control of the booking.</p>";
        $message .= "<p>You can view the detail of your booking here ".$url."</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Ziwa Tours Booking Reservation');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}