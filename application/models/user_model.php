<?php

class user_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function get_user_info($user_id)
    {
        $this->db->from("users");
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row_array();
    }

    public function get_all_users()
    {
        $this->db->from("users");
        $this->db->where('user_email !=', "info@ziwatours.biz");
        $this->db->order_by("user_id", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function update_user($user_id,$user_name,$user_last_name,$user_cell,$user_gender,$user_email,$user_country)
    {
        $data = array(
            'user_name' => $user_name,
            'user_last_name' => $user_last_name,
            'user_cell' => $user_cell,
            'user_gender' => $user_gender,
            'user_email' => $user_email,
            'user_country' => $user_country
        );

        $this->db->where('user_id', $user_id);
        $this->db->update('users', $data);
    }

   public function add_user($user_name,$user_last_name,$user_cell,$user_gender,$user_email,$user_country)
   {
       $data = array(
           'user_name' => $user_name,
           'user_last_name' => $user_last_name,
           'user_cell' => $user_cell,
           'user_gender' => $user_gender,
           'user_email' => $user_email,
           'user_country' => $user_country
       );

       $this->db->insert('users', $data);
   }

    public function update_user_status($user_id,$status)
    {
        $data = array(
            'user_status' => $status
        );

        $this->db->where('user_id', $user_id);
        $this->db->update('users', $data);
    }

    public function delete_user($u_id)
    {
        $this->db->where('user_id', $u_id);
        $this->db->delete('users');
    }
}