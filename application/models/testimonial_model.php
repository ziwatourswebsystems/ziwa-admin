<?php

class testimonial_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function get_testimonial_details($t_id)
    {
        $this->db->from("testimonials");
        $this->db->where('t_id', $t_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->row();
    }

    public function get_all_user_testimonial()
    {
        $this->db->from("testimonials");
        $this->db->order_by("t_date", "DESC");
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

   public function add_testimonial($headline,$author,$testimonial_body,$date)
   {
       $fdate=date('Y-m-d',strtotime($date));
       $data = array(
           't_headline' => $headline ,
           't_author' => $author,
           't_body' => $testimonial_body,
           't_date' => $fdate
       );

       $this->db->insert('testimonials', $data);
   }

    public function update_testimonial_image($t_id,$t_image)
    {
        $data = array(
            't_imge' => $t_image
        );

        $this->db->where('t_id', $t_id);
        $this->db->update('testimonials', $data);
    }

    public function delete_testimonial($t_id)
    {
        $this->db->where('t_id', $t_id);
        $this->db->delete('testimonials');
    }

    public function send_welcome_email($email,$user_name,$user_pass)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Welcome to Ziwa Tours and Thank you for choosing us as your travel guide. You are now an official member of Ziwa Tours</p>";
        $message .= "<p>Please check out your member dashboard where you can create, update booking as well as recieve exclusive specials and oofers from Ziwa Tours.</p>";
        $message .= "<p>You Login Details as username :".$email." password : ".$user_pass." to access your membership dashboard you can go to http://members.ziwatours.biz/</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Welcome to Ziwa Tours');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}