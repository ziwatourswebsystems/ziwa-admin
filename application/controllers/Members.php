<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        if(($this->session->userdata['user_role'] != 1)&&($this->session->userdata['user_role'] != 2))
        {
            redirect('auth/');
        }
        $this->load->model('auth_model','user');
        $this->load->model('booking_model','booking');
        $this->load->model('services_model','services');

        $user_update_totals_to = $this->booking->get_user_unread_updates_to($this->session->userdata['user_email']);
        $user_update_totals_from = $this->booking->get_user_unread_updates_from($this->session->userdata['user_email']);

        if(isset($user_update_totals_to[0]['type_count'])){
            $this->session->userdata['notifcation_count'] = $user_update_totals_to[0]['type_count'];
        }else {
            $this->session->userdata['notifcation_count'] = 0;
        }
        if(isset($user_update_totals_from[0]['type_count'])){
            $this->session->userdata['message_count'] = $user_update_totals_from[0]['type_count'];
        }else {
            $this->session->userdata['message_count'] = 0;
        }
    }

    public function index()
    {
        $booking_stats = $this->booking->get_booking_stats($this->session->userdata['user_email']);
        $data['booking_totals'] = $booking_stats;
        $update_records = $this->booking->get_latest_updates($this->session->userdata['user_email']);
        $data['update_records'] = $update_records;
        //print_r($data['booking_records']);
        //die;
        $data['user_name'] = $this->session->userdata['user_name'];
        $data['user_last_name'] = $this->session->userdata['user_last_name'];
        $data['user_cell'] = $this->session->userdata['user_cell'];
        $data['user_gender'] = $this->session->userdata['user_gender'];
        $data['user_country'] = $this->session->userdata['user_country'];
        $data['user_role'] = $this->session->userdata['user_role'];

        $data['private_services'] = $this->services->get_private_services();
        $data['package_services'] = $this->services->get_package_services();
        $data['tour_services'] = $this->services->get_tour_services();
        $data['exclusive_services'] = $this->services->get_exclusive_services();

        $data['layout'] = 'members';
        $data['main_content'] = '/members/index';
        $this->load->view('includes/template',$data);
    }

    public function addBooking()
    {
        $booking_data = $this->input->post(NULL, TRUE);

        if(count($booking_data) == 5)
        {
            $user_id= $this->session->userdata['user_email'];
            $booking_date = $booking_data['booking_date'];
            $booking_type_id = $booking_data['booking_type_id'];
            $booking_id = $booking_data['booking_id'];
            $booking_adults = $booking_data['booking_adults'];
            $booking_children = $booking_data['booking_children'];
            $this->booking->add_Booking($user_id,$booking_date,$booking_type_id,$booking_id,$booking_adults,$booking_children);
        }
    }

    public function bookings()
    {
        $booking_records = $this->booking->get_latest_booking($this->session->userdata['user_email']);
        $data['booking_records'] = $booking_records;
        $data['user_role'] = $this->session->userdata['user_role'];
        $data['layout'] = 'members';
        $data['main_content'] = '/members/bookings';
        $this->load->view('includes/template',$data);
    }

    public function enquire()
    {
        $data['user_role'] = $this->session->userdata['user_role'];
        $data['layout'] = 'members';
        $data['main_content'] = '/members/enquire';
        $this->load->view('includes/template',$data);
    }

    public function view_message($message_id = 0)
    {
        if($message_id != 0) {

            $message_details = $this->booking->get_message_details($message_id);
            //print_r($message_details);
            $this->booking->update_message_read_status($message_id);

            $user_update_totals_to = $this->booking->get_user_unread_updates_to($this->session->userdata['user_email']);
            $user_update_totals_from = $this->booking->get_user_unread_updates_from($this->session->userdata['user_email']);

            if(isset($user_update_totals_to[0]['type_count'])){
                $this->session->userdata['notifcation_count'] = $user_update_totals_to[0]['type_count'];
            }else {
                $this->session->userdata['notifcation_count'] = 0;
            }
            if(isset($user_update_totals_from[0]['type_count'])){
                $this->session->userdata['message_count'] = $user_update_totals_from[0]['type_count'];
            }else {
                $this->session->userdata['message_count'] = 0;
            }

            $all_message_records = $this->booking->get_all_user_updates($this->session->userdata['user_email']);
            $data['all_message_records'] = $all_message_records;
            $data['message_details'] = $message_details;
            $data['user_role'] = $this->session->userdata['user_role'];
            $data['layout'] = 'members';
            $data['main_content'] = '/members/viewmessage';
            $this->load->view('includes/template', $data);
        }
        else
        {

            $data['message_details'] = "list";
            $all_message_records = $this->booking->get_all_user_updates($this->session->userdata['user_email']);
            $data['all_message_records'] = $all_message_records;
            $data['user_role'] = $this->session->userdata['user_role'];
            $data['layout'] = 'members';
            $data['main_content'] = '/members/viewmessage';
            $this->load->view('includes/template', $data);

        }
    }

    public function view_booking($booking_id = 0)
    {
        $booking_pieces = explode("_", $booking_id);
        if(count($booking_pieces) == 2) {
            $booking_key = $booking_pieces[0];
            $booking_token = $booking_pieces[1];
            $data['booking_info'] = $this->booking->get_booking_info($booking_key,$booking_token);
            $booking_info = $data['booking_info'];
            if(count($data['booking_info']) == 1)
            {
                $data['user_role'] = $this->session->userdata['user_role'];
                $btype_data = $this->booking->get_book_type($booking_info->booking_type_id);
                $data['book_type'] = $btype_data->booking_type_name;
                $data['booking_name'] = $this->booking->get_book_name($booking_info->booking_id,$btype_data->booking_from);
                $data['layout'] = 'members';
                $data['main_content'] = '/members/viewbooking';
                $this->load->view('includes/template', $data);
            }
            else
            {
                //erro because no record was found
            }
        }
        else
        {
            // error because invalid data was submited
        }
    }

    public function acceptBooking()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if((count($user_data) == 1)&&(isset($user_data['booking_id'])))
        {
            $this->booking->accept_booking($user_data['booking_id']);
        }
    }

    public function cancelBooking()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if((count($user_data) == 1)&&(isset($user_data['booking_id'])))
        {
            $this->booking->cancel_booking($user_data['booking_id']);
        }
    }


    public function sendMessage()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 2)
        {
            $from = $this->session->userdata['user_email'];
            $to = "info@ziwatours.biz";
            $message_type = "1";
            $message_parent = 0;
            $subject = $user_data["enquiry_subject"];
            $message_body = $user_data["enquiry_body"];
            $this->user->SendZiwa_Message($from,$to,$message_type,$subject,$message_body,$message_parent);
            echo "success";
        }
        else
        {
            echo "fail";
            //redirect('members/profile/'.$this->session->userdata['user_id']);
        }
    }

    public function saveUserProfile()
    {
        $user_data = $this->input->post(NULL, TRUE);
        echo count($user_data);
        if(count($user_data) == 6)
        {
            $user_id = $user_data["frm_profile_user_id"];
            $fname = $user_data["fname"];
            $sname = $user_data["sname"];
            $celln = $user_data["celln"];
            $gender = $user_data["ugender"];
            $country = $user_data["country"];
            if($user_id = $this->session->userdata['user_email'])
            {
                $this->user->updateUserProfile($user_id,$fname,$sname,$celln,$gender,$country);
                echo "success";
                $this->session->userdata['user_name'] = $fname;
                $this->session->userdata['user_last_name'] = $sname;
                $this->session->userdata['user_cell'] = $celln;
                $this->session->userdata['user_gender'] = $gender;
                $this->session->userdata['user_country'] = $country;
            }
        }
        else
        {
            echo "fail";
            //redirect('members/profile/'.$this->session->userdata['user_id']);
        }
    }

    public function changeUserPassword()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 2)
        {
            //print_r($user_data);
            $user_id = $user_data['use_id'];
            $user_password = $user_data['use_password'];
            if($user_id = $this->session->userdata['user_email'])
            {
                $this->user->updateUserPassword($user_id,$user_password);
                echo "success";
            }

        }
        else
        {
            redirect('members/profile/'.$this->session->userdata['user_id']);
        }
    }

    public function profile($user_id = 0)
    {
        if($user_id == 0)
        {
            redirect('members/');
        }
        else
        {
            if($user_id == $this->session->userdata['user_id']) {
                $data['user_role'] = $this->session->userdata['user_role'];
                $data['user_email'] = $this->session->userdata['user_email'];
                $data['user_name'] = $this->session->userdata['user_name'];
                $data['user_last_name'] = $this->session->userdata['user_last_name'];
                $data['user_cell'] = $this->session->userdata['user_cell'];
                $data['user_gender'] = $this->session->userdata['user_gender'];
                $data['user_country'] = $this->session->userdata['user_country'];

                $data['layout'] = 'members';
                $data['main_content'] = '/members/profile';
                $this->load->view('includes/template', $data);
            }
            else
            {
                redirect('members/profile/'.$this->session->userdata['user_id']);
            }
        }
     }



}
