<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        if($this->session->userdata['user_role'] != 2)
        {
            redirect('auth/');
        }
        $this->load->model('testimonial_model','testimonial');
        $this->load->model('services_model','services');
        $this->load->model('user_model','user');
        $this->load->model('tours_model','tours');
        $this->load->model('blogs_model','blogs');
        $this->load->model('message_model','message');
        $this->load->model('booking_model','booking');
        $user_update_totals_to = $this->booking->get_user_unread_updates_to($this->session->userdata['user_email']);
        $user_update_totals_from = $this->booking->get_user_unread_updates_from($this->session->userdata['user_email']);

        if(isset($user_update_totals_to[0]['type_count'])){
            $this->session->userdata['notifcation_count'] = $user_update_totals_to[0]['type_count'];
        }else {
            $this->session->userdata['notifcation_count'] = 0;
        }
        if(isset($user_update_totals_from[0]['type_count'])){
            $this->session->userdata['message_count'] = $user_update_totals_from[0]['type_count'];
        }else {
            $this->session->userdata['message_count'] = 0;
        }

    }

    public function index()
    {
        $data['live_stats'] = $this->booking->get_live_totals();
        $data['update_totals'] = $this->booking->get_count_enquiries();
        $data['booking_totals'] = $this->booking->get_booking_stats_all();
        $data['booking_records'] = $this->booking->get_latest_booking_all();
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/index';
        $this->load->view('includes/template',$data);
    }

    public function bookings()
    {
        $booking_records = $this->booking->get_latest_booking_all();
        $data['booking_records'] = $booking_records;
        $data['user_role'] = $this->session->userdata['user_role'];

        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/bookings';
        $this->load->view('includes/template',$data);
    }

    public function acceptBooking()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if((count($user_data) == 1)&&(isset($user_data['booking_id'])))
        {
            $this->booking->accept_booking_admin($user_data['booking_id']);
        }
    }


    public function cancelBooking()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if((count($user_data) == 1)&&(isset($user_data['booking_id'])))
        {
            $this->booking->cancel_booking_admin($user_data['booking_id']);
        }
    }


    public function updateBooking()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if(count($user_data) == 5)
        {

            $booking_id = $user_data['booking_id'];
            $adults = $user_data['num_of_adults'];
            $children = $user_data['num_of_children'];
            $book_date = $user_data['booking_date'];
            $book_status = $user_data['booking_status'];
            // update booking
            $this->booking->update_booking_details($booking_id,$adults,$children,$book_date,$book_status);

            // get user id
            $booking_data = $this->booking->get_booking_user($booking_id);
            $user_email = $booking_data->user_id;
            $token = $booking_data->transaction_token;

            // add update about booking that was modified
            $parent = 0;
            $subject = "Booking Update";
            $to = $user_email;
            $code = $booking_id.'_'.$token;
            $body = 'Your Booking details have been updated to view details click this link : <a href="/members/view_booking/'.$code.'">Updated Booking Details</a>';
           // die($body);
            $this->message->add_message($parent,$subject,$to,$body);
        }
    }

    public function view_booking($booking_id = 0)
    {
        $booking_pieces = explode("_", $booking_id);
        if(count($booking_pieces) == 2) {
            $booking_key = $booking_pieces[0];
            $booking_token = $booking_pieces[1];
            $data['booking_info'] = $this->booking->get_booking_info($booking_key,$booking_token);
            $booking_info = $data['booking_info'];
            if(count($data['booking_info']) == 1)
            {
                $data['user_role'] = $this->session->userdata['user_role'];
                $btype_data = $this->booking->get_book_type($booking_info->booking_type_id);
                $data['book_type'] = $btype_data->booking_type_name;
                $data['booking_name'] = $this->booking->get_book_name($booking_info->booking_id,$btype_data->booking_from);
                $data['layout'] = 'admin';
                $data['main_content'] = '/admin/viewbooking';
                $this->load->view('includes/template', $data);
            }
            else
            {
                //erro because no record was found
            }
        }
        else
        {
            // error because invalid data was submited
        }
    }


    public function message($message_parent_id = 0)
    {
        if($message_parent_id != 0) {
            $data['message_details'] = $this->message->get_message_details($message_parent_id);
        }

        $data['messages'] = $this->message->get_all_messages();
        //print_r($data['messages']);
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/message';
        $this->load->view('includes/template',$data);
    }

    public function addmessage()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 4)
        {
            $parent = $user_data['message_parent'];
            $subject = $user_data['message_subject'];
            $to = $user_data['message_to'];
            $body = $user_data['message_body'];
            $this->message->add_message($parent,$subject,$to,$body);
        }
    }

    public function members($action = "add",$user_id = 0)
    {
        if($action = "edit") {
            $data['user_info'] = $this->user->get_user_info($user_id);
        }

        $data['action'] = $action;
        $data['users'] = $this->user->get_all_users();
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/members';
        $this->load->view('includes/template',$data);
    }

    public function export_members()
    {
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Members_".date('M.j.y').".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');
        fputcsv($handle, array(
            'Members ID',
            'First Name',
            'Last Name',
            'Email',
            'Cell',
            'Date Created'
        ));


        $members = $this->user->get_all_users();
        foreach ($members as $member) {
            fputcsv($handle, array(
                $member['user_id'],
                $member['user_name'],
                $member['user_last_name'],
                $member['user_email'],
                $member['user_cell'],
                $member['date_created']
            ));
        }

        fclose($handle);
        exit;
    }

    public function addmember()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) > 1)
        {
            // if params match do add
            $email = $user_data['uemail'];
            $fname = $user_data['ufname'];
            $lname = $user_data['usname'];
            $cell = $user_data['ucelln'];
            $gender = $user_data['ugender'];
            $country = $user_data['ucountry'];
            $this->user->add_user($fname,$lname,$cell,$gender,$email,$country);
        }
    }

    public function updatemember()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) > 1)
        {
            // if params match do add
            $userid = $user_data['userid'];
            $email = $user_data['uemail'];
            $fname = $user_data['ufname'];
            $lname = $user_data['usname'];
            $cell = $user_data['ucelln'];
            $gender = $user_data['ugender'];
            $country = $user_data['ucountry'];
            $this->user->update_user($userid,$fname,$lname,$cell,$gender,$email,$country);
        }
    }

    public function deleteMember()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 1)
        {
            //print_r($user_data);
            $m_id = $user_data['m-id'];
            $this->user->delete_user($m_id);
        }
    }

    public function services()
    {
        $data['services'] = $this->services->get_all_services();
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/services';
        $this->load->view('includes/template',$data);
    }

    public function addservice()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 2)
        {
            //print_r($user_data);
            $servicename = $user_data['servicename'];
            $service_body = $user_data['service_body'];
            $this->services->add_service($servicename,$service_body);
        }
    }

    public function deleteService()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 1)
        {
            //print_r($user_data);
            $s_id = $user_data['s-id'];
            $this->services->delete_service($s_id);
        }
    }


    public function blog()
    {
        $data['blogs'] = $this->blogs->get_all_blogs();
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/blog';
        $this->load->view('includes/template',$data);
    }

    public function addblog()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if(count($user_data) == 4)
        {
            //print_r($user_data);
            $blog_name = $user_data['blog_name'];
            $blog_body = $user_data['blog_aticle'];
            $blog_summary = $user_data['blog_summary'];
            $blog_tags = $user_data['blog_tags'];
            //print_r($blog_tags);
            $this->blogs->add_blog($blog_name,$blog_body,$blog_summary,$blog_tags);
        }

    }

    public function deleteBlog()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 1)
        {
            $t_id = $user_data['t-id'];
            $this->blogs->delete_blog($t_id);
        }
    }

    public function blog_mainUpload($t_id)
    {
        $data['t_id'] =  $t_id;
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/blogmainupload';
        $this->load->view('includes/template',$data);

    }

    public function doBlogMainUpload($t_id)
    {
        if (empty($_FILES) || $_FILES["file"]["error"]) {
            die('{"OK": 0}');
        }

        $fileName = $_FILES["file"]["name"];

        mkdir("assets/custom/uploads/blog/".$t_id."/main", 0777, TRUE);
        //die($t_id,$fileName);
        $this->blogs->update_blog_main_image($t_id,$fileName);
        move_uploaded_file($_FILES["file"]["tmp_name"], "assets/custom/uploads/blog/$t_id/main/$fileName");

        die('{"OK": 1}');
    }


    public function tours($method = "none",$t_id = 0,$t_value = 0)
    {
        if(($method == "special")||($method == "exclusive")){
            if($method == "special"){
                $this->tours->update_tour_special($t_id,$t_value);
            }else{
               $this->tours->update_tour_exclusive($t_id,$t_value);
            }
        }
        $data['tour'] = 0;
        if($method == "edit")
        {
            $data['tour'] = $this->tours->get_tour_detail($t_id);
        }
        $data['tours'] = $this->tours->get_all_tours();
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/tours';
        $this->load->view('includes/template',$data);

    }

    public function addtour()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if(count($user_data) > 5)
        {
            //print_r($user_data);
            $tour_name = urlencode ($user_data['tour_name']);
            $tour_overview = urlencode ($user_data['tour_overview']);
            $tour_about = urlencode ($user_data['tour_about']);
            $is_special = $user_data['tour_special'];
            $is_exclusive = $user_data['tour_exclusive'];
            $tour_tags = $user_data['tour_tags'];

            $this->tours->add_tour($tour_name,$tour_overview,$tour_about,$is_special,$is_exclusive,$tour_tags);
        }
    }

    public function edittour()
    {
        $user_data = $this->input->post(NULL, TRUE);

        if(count($user_data) > 5)
        {

            $tour_id = $user_data['tour_id'];
            $tour_name = urlencode ($user_data['tour_name']);
            $tour_overview = urlencode ($user_data['tour_overview']);
            $tour_about = urlencode ($user_data['tour_about']);
            $is_special = $user_data['tour_special'];
            $is_exclusive = $user_data['tour_exclusive'];
            $tour_tags = $user_data['tour_tags'];

            $this->tours->update_tour($tour_id,$tour_name,$tour_overview,$tour_about,$is_special,$is_exclusive,$tour_tags);
        }
    }

    public function deleteTour()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 1)
        {
            $t_id = $user_data['t-id'];
            $this->tours->delete_tour($t_id);
        }
    }

    public function tours_mapUpload($t_id)
    {
        $data['t_id'] =  $t_id;
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/toursmapupload';
        $this->load->view('includes/template',$data);

    }



    public function tours_mainUpload($t_id)
    {
        $data['t_id'] =  $t_id;
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/toursmainupload';
        $this->load->view('includes/template',$data);

    }

    public function tours_galUpload($t_id)
    {
        $data['t_id'] =  $t_id;
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/toursgalupload';
        $this->load->view('includes/template',$data);

    }

    public function doToursMapUpload($t_id)
    {
        if (empty($_FILES) || $_FILES["file"]["error"]) {
            die('{"OK": 0}');
        }

        $fileName = $_FILES["file"]["name"];

        mkdir("assets/custom/uploads/tours/".$t_id."/map", 0777, TRUE);
        //die($t_id,$fileName);
        $this->tours->update_tour_map($t_id,$fileName);
        move_uploaded_file($_FILES["file"]["tmp_name"], "assets/custom/uploads/tours/$t_id/map/$fileName");

        die('{"OK": 1}');
    }


    public function doToursGalUpload($t_id)
    {
        if (empty($_FILES) || $_FILES["file"]["error"]) {
            die('{"OK": 0}');
        }

        $fileName = $_FILES["file"]["name"];
        //die($t_id,$fileName);
        $path = "assets/custom/uploads/tours/$t_id/$fileName";
        if(!file_exists($path)) {
            mkdir("assets/custom/uploads/tours/".$t_id, 0777, TRUE);
            $this->tours->update_tour_images($t_id, $fileName);
            move_uploaded_file($_FILES["file"]["tmp_name"], "assets/custom/uploads/tours/$t_id/$fileName");
            die('{"OK": 1}');
        }

    }

    public function doToursMainUpload($t_id)
    {
        if (empty($_FILES) || $_FILES["file"]["error"]) {
            die('{"OK": 0}');
        }

        $fileName = $_FILES["file"]["name"];

        mkdir("assets/custom/uploads/tours/".$t_id."/main", 0777, TRUE);
        //die($t_id,$fileName);
        $this->tours->update_tour_main_image($t_id,$fileName);
        move_uploaded_file($_FILES["file"]["tmp_name"], "assets/custom/uploads/tours/$t_id/main/$fileName");

        die('{"OK": 1}');
    }

    public function testimonials()
    {
        $data['testimonials'] = $this->testimonial->get_all_user_testimonial();
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/testimonials';
        $this->load->view('includes/template',$data);
    }

    public function doTestimonialUpload($t_id)
    {
        if (empty($_FILES) || $_FILES["file"]["error"]) {
            die('{"OK": 0}');
        }

        $fileName = $_FILES["file"]["name"];
        //die($t_id,$fileName);
        $this->testimonial->update_testimonial_image($t_id,$fileName);
        move_uploaded_file($_FILES["file"]["tmp_name"], "assets/uploads/testimonials/$fileName");

        die('{"OK": 1}');
    }

    public function testimonialsUpload($t_id)
    {
        $data['t_id'] =  $t_id;
        $data['layout'] = 'admin';
        $data['main_content'] = '/admin/testimonialsupload';
        $this->load->view('includes/template',$data);
    }



    public function deleteTestiumonial()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 1)
        {
            $t_id = $user_data['t-id'];
            $this->testimonial->delete_testimonial($t_id);
        }
    }

    public function addTestimonial()
    {
        $user_data = $this->input->post(NULL, TRUE);
        if(count($user_data) == 4)
        {
            $headline = $user_data['headline'];
            $author = $user_data['author'];
            $testimonial_body = $user_data['testimonial_body'];
            $date = $user_data['testimonial_date'];
            $this->testimonial->add_testimonial($headline,$author,$testimonial_body,$date);
        }
    }

    public function home_cms()
    {

    }

}
