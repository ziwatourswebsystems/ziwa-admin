<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model','user');
    }

    public function index()
    {
        $data['layout'] = 'auth';
        $data['main_content'] = '/auth/index';
        $this->load->view('includes/template',$data);
    }

    public function logout()
    {
        //$this->session->unset_userdata();
        $this->session->sess_destroy();
        redirect('auth/');
    }

    public function check_creds()
    {
        $user_cred_data = $this->input->post(NULL, TRUE);
        //print_r($user_cred_data);
        if(count($user_cred_data) == 2)
        {
            $user_name = $user_cred_data['username'];
            $user_password = $user_cred_data['user_password'];
            $user_data = $this->user->check_user_creds($user_name,$user_password);
            if($user_data)
            {
                //print_r($user_data);
                $this->session->set_userdata($user_data);
                if($this->session->userdata['user_role'] == 1)
                {
                    redirect('members/');
                }
                else if($this->session->userdata['user_role'] == 2)
                {
                    redirect('admin/');
                }
                else
                {
                    redirect('auth/');
                }
            }
            else
            {
                redirect('auth/index/error');
            }
        }
        else
        {
            die("Bad Request if problem persists please email info@ziwatours.biz");
        }
    }


}
