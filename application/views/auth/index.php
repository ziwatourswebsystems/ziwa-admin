<div id="mws-login-wrapper">
    <div id="mws-login">
        <h1>Login</h1>
        <div class="mws-login-lock"><i class="icon-lock"></i></div>
        <div id="mws-login-form">
            <form class="mws-form" id="ziwa_login_frm" name="ziwa_login_frm" action="/auth/check_creds" method="post">
                <div class="mws-form-row">
                    <div class="mws-form-item">
                        <input type="text" id="username" name="username" class="mws-login-username required" placeholder="username">
                    </div>
                </div>
                <div class="mws-form-row">
                    <div class="mws-form-item">
                        <input type="password" id="user_password" name="user_password" class="mws-login-password required" placeholder="password">
                    </div>
                </div>
                <div id="mws-login-remember" class="mws-form-row mws-inset">
                    <ul class="mws-form-list inline">
                        <li>
                            <input id="remember" type="checkbox">
                            <label for="remember">Remember me</label>
                        </li>
                    </ul>
                </div>
                <div class="mws-form-row">
                    <input type="submit" value="Login" class="btn btn-success mws-login-button">
                </div>
            </form>
        </div>
    </div>
</div>