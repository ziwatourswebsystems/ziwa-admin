<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assests/plugins/jgrowl/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/members.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        if($user_role == 2)
        {
            $this->load->view("includes/admin/adminnav");
        }
        else {
            $this->load->view("includes/members/membernav");
        }
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">

            <!-- Statistics Button Container -->
            <div class="mws-stat-container clearfix">


                <?php
                //print_r($booking_totals[0]);
                $index_count = 0;
                foreach($booking_totals as $booking_totals_item)
                {
                    $name = $booking_totals_item["booking_type_name"];
                    $count = $booking_totals_item["type_count"];

                    $icons = ['icol32-user-suit','icol32-package','icol32-car','icol32-gift-add'];
                ?>
                    <!-- Statistic Item -->
                    <a class="mws-stat" href="#">
                        <!-- Statistic Icon (edit to change icon) -->
                        <span class="mws-stat-icon <?php echo $icons[$index_count]; ?>"></span>

                        <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title"><?php echo $name; ?></span>
                            <span class="mws-stat-value"><?php echo $count; ?></span>
                        </span>
                    </a>

                <?php
                    ++$index_count;
                }

                ?>

            </div>

            <!-- Panels Start -->

            <div class="clear"></div>

            <div id="add_booking_msg" class="mws-form-message success" style="display:none"></div>


            <div class="clear"></div>

            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-calendar"></i>Quick Booking</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form class="mws-form" action="#">
                        <div class="mws-form-inline">
                            <div class="mws-form-row">
                                <label class="mws-form-label">Date for Booking</label>
                                <div class="mws-form-item">
                                    <div id="date_from" class="mws-datepicker"></div>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Booking Type</label>
                                <div class="mws-form-item">
                                    <select id="service_selector" class="large">
                                        <option value="1">Private Chauffeur</option>
                                        <option value="2">Package Deal</option>
                                        <option value="3">Tour</option>
                                        <option value="4">Exclusive</option>
                                    </select>
                                </div>
                            </div>

                            <div id="panel_service_1" class="mws-form-row" >
                                <label class="mws-form-label">Selected Service</label>
                                <div class="mws-form-item">
                                    <select id="select_service_1" class="large">
                                    <?php
                                    foreach($private_services as $item) {
                                    ?>
                                    <option value="<?php echo $item['service_id']; ?>"><?php echo $item['service_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            <div id="panel_service_2" class="mws-form-row" style="display:none">
                                <label class="mws-form-label">Selected Service</label>
                                <div class="mws-form-item">
                                    <select id="select_service_2" class="large">
                                        <?php
                                        foreach($package_services as $item) {
                                            ?>
                                            <option value="<?php echo $item['service_id']; ?>"><?php echo $item['service_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div id="panel_service_3" class="mws-form-row" style="display:none">
                                <label class="mws-form-label">Selected Service</label>
                                <div class="mws-form-item">
                                    <select id="select_service_3" class="large">
                                        <?php
                                        foreach($tour_services as $item) {
                                            ?>
                                            <option value="<?php echo $item['tour_id']; ?>"><?php echo $item['tour_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div id="panel_service_4" class="mws-form-row" style="display:none">
                                <label class="mws-form-label">Selected Service</label>
                                <div class="mws-form-item">
                                    <select id="select_service_4" class="large">
                                        <?php
                                        foreach($exclusive_services as $item) {
                                            ?>
                                            <option value="<?php echo $item['tour_id']; ?>"><?php echo $item['tour_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Number of Adults</label>
                                <div class="mws-form-item">
                                    <select id="adults_sel" class="large">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Number of Children</label>
                                <div class="mws-form-item">
                                    <select id="children_sel" class="large">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="mws-button-row">
                                <input id="member_add_booking_btn" type="submit" value="Request Booking" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Tours Updates</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>From</th>
                                <th>Type</th>
                                <th>Subject</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            if(count($update_records) > 0) {

                                foreach($update_records as $update_record_item) {
                                    $url = "/members/view_message/".$update_record_item['update_id'];
                                    $message_types = [1=>"Message",2=>"Notification"]
                                ?>

                                    <tr class="odd">
                                        <td class=" sorting_1"><?php echo $update_record_item['update_from']; ?></td>
                                        <td class=" "><?php echo $message_types[$update_record_item['update_type']]; ?></td>
                                        <td class=" "><?php echo $update_record_item['update_subject'] ?></td>
                                        <td class=" ">
                                        <span class="btn-group">
                                        </span>
                                            <a href="<?php echo $url; ?>" class="btn_activity_view btn btn-small"><i
                                                    class="icon-eye-open"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            else
                            {
                            ?>
                                <tr>
                                    <td></td>
                                    <td class=" sorting_1" >No Data Found</td>
                                    <td></td>
                                </tr>

                            <?php
                            }
                            ?>

                            </tbody></table>
                    </div></div>

            <div class="clear"></div>


            <!-- Panels End -->
        </div>
        <!-- Inner Container End -->

        <!-- Footer -->
        <div id="mws-footer">
            Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
        </div>

    </div>
    <!-- Main Container End -->

</div>
