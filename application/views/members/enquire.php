<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assests/plugins/jgrowl/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/members.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        if($user_role == 2)
        {
            $this->load->view("includes/admin/adminnav");
        }
        else {
            $this->load->view("includes/members/membernav");
        }
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">


            <div class="clear"></div>

            <div id="mws_enquiry_form_msg" class="mws-form-message success" style="display:none"></div>

            <div class="clear"></div>


            <div class="mws-panel grid_8">
                <div class="mws-panel-header">
                    <span>Ziwa Tours Enquiry</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_member_enquiry" name="frm_member_enquiry" class="mws-form" action="#">
                        <div class="mws-form-inline">

                            <div class="mws-form-row">
                                <label class="mws-form-label">Enquiry Type</label>
                                <div class="mws-form-item">
                                    <select id="enquiry_subject" name="enquiry_subject" class="large">
                                        <option value="General Enquiry">General Enquiry</option>
                                        <option value="Tour Enquiry">Tour Enquiry</option>
                                        <option value="Package Enquiry">Package Enquiry</option>
                                        <option value="Private Chauffeur Enquiry">Private Chauffeur Enquiry</option>
                                        <option value="Exclusive Enquiry">Exclusive Enquiry</option>
                                        <option value="Billing Enquiry">Billing Enquiry</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mws-form-row">
                                <label class="mws-form-label">Enquiry Body</label>
                                <div class="mws-form-item">
                                    <textarea id="enquiry_body" name="enquiry_body" rows="" cols="" class="large"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="mws-button-row">
                            <input id="btn_member_enquiry" name="btn_member_enquiry" type="submit" value="Submit Enquiry" class="btn btn-danger">
                            <input type="reset" value="Reset" class="btn ">
                        </div>
                    </form>
                </div>
            </div>


            <div class="clear"></div>


            <!-- Panels End -->
        </div>
        <!-- Inner Container End -->

        <!-- Footer -->
        <div id="mws-footer">
            Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
        </div>

    </div>
    <!-- Main Container End -->

</div>
