<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assests/plugins/jgrowl/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/members.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        if($user_role == 2)
        {
            $this->load->view("includes/admin/adminnav");
        }
        else {
            $this->load->view("includes/members/membernav");
        }
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">


            <div class="clear"></div>

            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span>Profile Information</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_user_profile" class="mws-form" >
                        <input id="frm_profile_user_id" name="frm_profile_user_id" type="hidden" value="<?php echo $user_email; ?>" >
                        <div id="user_profile_mws-validate-error" class="mws-form-message error" style="display:none;"></div>
                        <div class="mws-form-inline">
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Name</label>
                                <div class="mws-form-item">
                                    <input id="fname" name="fname" type="text" value="<?php echo $user_name; ?>" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Surname</label>
                                <div class="mws-form-item">
                                    <input id="sname" name="sname" type="text" value="<?php echo $user_last_name; ?>" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Cell Number</label>
                                <div class="mws-form-item">
                                    <input id="celln" name="celln" type="text" value="<?php echo $user_cell; ?>" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Gender</label>
                                <?php

                                $male = "";
                                $female = "";

                                if($user_gender == 1)
                                {
                                    $male = "selected";
                                }
                                else
                                {
                                    $female = "selected";
                                }

                                ?>
                                <div class="mws-form-item">
                                    <select id="ugender" name="ugender" class="large">
                                        <option value="1" <?php echo $male; ?> >Male</option>
                                        <option value="2" <?php echo $female; ?> >Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Country of Passport</label>
                                <div class="mws-form-item">
                                    <input id="country" name="country" type="text" value="<?php echo $user_country; ?>" class="large">
                                </div>
                            </div>
                        </div>
                        <div class="mws-button-row">
                            <input id="memeber_save_profile" type="submit" value="Save Profile Info" class="btn btn-danger">
                        </div>
                    </form>
                </div>
            </div>

            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span>User Account Access</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_user_password" name="frm_teacher_password" class="mws-form" action="#">
                        <input id="password_user_id" type="hidden" value="<?php echo $user_email; ?>" >
                        <div id="mws-password-validate-error" class="mws-form-message error" style="display:none;">

                        </div>
                        <fieldset class="mws-form-inline">
                            <legend>Set New Password</legend>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">New Password</label>
                                <div class="mws-form-item">
                                    <input id="opass"  name="opass" type="password" class="large">
                                </div>
                            </div>
                        </fieldset>
                        <div class="mws-button-row">
                            <input id="memeber_save_password" name="memeber_save_password" type="submit" value="Save Password" class="btn btn-danger">
                        </div>
                    </form>
                </div>
            </div>

            <div class="clear"></div>


            <!-- Panels End -->
        </div>
        <!-- Inner Container End -->

        <!-- Footer -->
        <div id="mws-footer">
            Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
        </div>

    </div>
    <!-- Main Container End -->

</div>
