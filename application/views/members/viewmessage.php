<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assests/plugins/jgrowl/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/members.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        if($user_role == 2)
        {
            $this->load->view("includes/admin/adminnav");
        }
        else {
            $this->load->view("includes/members/membernav");
        }
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">


            <div class="clear"></div>
            <?php
            $updatetype = [1=>"Message",2=>"Notification"];
            if($message_details != "list")
            {
            ?>
                <div class="mws-panel grid_8">
                    <div class="mws-panel-header">
                        <span><i class="icon-file-xml"></i> Ziwa Tour System [ <?php echo $updatetype[$message_details->update_type]; ?> ] </span>
                    </div>
                    <div class="mws-panel-body">
                        <table class="mws-table">
                            <thead>
                            <tr>
                                <th>Sent From</th>
                                <th>Sent To</th>
                                <th>Sent Date</th>
                                <th>Status</th>
                                <th>Subject</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $status = ['Pending', 'Open', 'Reviewed', 'Closed']
                            ?>
                            <tr>
                                <td><?php echo $message_details->update_from; ?></td>
                                <td><?php echo $message_details->update_to; ?></td>
                                <td><?php echo $message_details->update_date; ?></td>
                                <td><?php echo $status[$message_details->update_status]; ?></td>
                                <td><?php echo $message_details->update_subject; ?></td>
                            </tr>

                            </tbody>
                        </table>

                        <pre>
                            <p><?php echo $message_details->update_content; ?></p>
                        </pre>
                    </div>
                </div>
            <?php
            }
            ?>
            <div class="clear"></div>


            <div class="mws-panel grid_8 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Message History</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>Type</th>
                                <th>From</th>
                                <th>Status</th>
                                <th>Subject</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">

                            <?php
                            $read_types = [0=>"Not Read",1=>"Read Already"];
                            foreach($all_message_records as $all_message_records_item) {
                            //print_r($all_message_records_item);
                            ?>
                                <tr class="odd">
                                    <td class=" "><?php echo $updatetype[$all_message_records_item['update_type']]; ?></td>
                                    <td class=" "><?php echo $all_message_records_item['update_from']; ?></td>
                                    <td class=" "><?php echo $read_types[$all_message_records_item['update_read']]; ?></td>
                                    <td class=" "><?php echo $all_message_records_item['update_subject']; ?></td>
                                    <td class=" ">
                                        <span class="btn-group">
                                        </span>
                                        <a href="/members/view_message/<?php echo $all_message_records_item['update_id']; ?>" class="btn_activity_view btn btn-small"><i
                                                class="icon-eye-open"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>


                            </tbody></table>
                    </div></div>

            <!-- Panels End -->
        </div>
        <!-- Inner Container End -->

        <!-- Footer -->
        <div id="mws-footer">
            Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
        </div>

    </div>
    <!-- Main Container End -->

</div>
