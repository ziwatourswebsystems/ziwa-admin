<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>

            <div id="mws_services_form_msg" class="mws-form-message success" style="display:none"></div>


            <div class="clear"></div>

            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-calendar"></i>Add Ziwa Services</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_services" class="mws-form" action="#">
                        <div class="mws-form-inline">

                            <div class="mws-form-row">
                                <label class="mws-form-label">Service Name</label>
                                <div class="mws-form-item">
                                    <input id="servicename" name="servicename" type="text" value="" class="large" />
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Service Body</label>
                                <div class="mws-form-item">
                                    <textarea row="30" id="service_body" name="service_body" class="large">

                                    </textarea>
                                </div>
                            </div>

                            <div class="mws-button-row">
                                <input id="btn_service_add" type="submit" value="Add Service" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Services Stats</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                        <thead>
                        <tr>
                            <th>Total Services</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center"><?php echo "<h4>".count($services)."</h4>"; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mws-panel grid_4 mws-collapsible">

                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Services</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>Name</th>
                                <th>Status</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            if(count($services) > 0) {
                                $active_state = ["Active","InActive"];
                                foreach($services as $item) {
                                    ?>
                                    <tr class="odd">
                                        <td class=" sorting_1"><?php echo $item['service_name']; ?></td>
                                        <td class=" "><?php echo $active_state[$item['is_active']]; ?></td>
                                        <td class=" " width="30%">
                                        <span class="btn-group">
                                                    </span>
                                            <a rel="popover" data-trigger="hover" data-placement="left" data-content="<?php echo $item['service_content']; ?>" data-original-title="<?php echo $item['service_name']; ?>" href="#" class="btn_activity_view btn btn-small"><i
                                                    class="icon-eye-open"></i></a>
                                            <a rel="tooltip" data-original-title="Delete Service Details" id="lnk_delete_service" data-s-id="<?php echo $item['service_id']; ?>" href="#" class="btn_activity_view btn btn-small"><i
                                                    class="icon-remove"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->


    </div>
