<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>

            <div id="mws_member_form_msg" class="mws-form-message success" style="display:none"></div>

            <div class="clear"></div>

            <?php
            if($action == "edit")
            {
            ?>

                <div class="mws-panel grid_4 mws-collapsible">
                    <div class="mws-panel-header">
                        <span><i class="icon-calendar"></i>Quick Member Update</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <form id="frm_member_update" class="mws-form" action="#">
                            <div class="mws-form-inline">
                                <input id="userid" name="userid" type="hidden" value="<?php echo $user_info['user_id']; ?>" class="large">
                                <div class="mws-form-row bordered">
                                    <label class="mws-form-label">Email</label>
                                    <div class="mws-form-item">
                                        <input id="uemail" name="uemail" type="text" value="<?php echo $user_info['user_email']; ?>" class="large">
                                    </div>
                                </div>
                                <div class="mws-form-row bordered">
                                    <label class="mws-form-label">Name</label>
                                    <div class="mws-form-item">
                                        <input id="ufname" name="ufname" type="text" value="<?php echo $user_info['user_name']; ?>" class="large">
                                    </div>
                                </div>
                                <div class="mws-form-row bordered">
                                    <label class="mws-form-label">Surname</label>
                                    <div class="mws-form-item">
                                        <input id="usname" name="usname" type="text" value="<?php echo $user_info['user_last_name']; ?>" class="large">
                                    </div>
                                </div>
                                <div class="mws-form-row bordered">
                                    <label class="mws-form-label">Cell Number</label>
                                    <div class="mws-form-item">
                                        <input id="ucelln" name="ucelln" type="text" value="<?php echo $user_info['user_cell']; ?>" class="large">
                                    </div>
                                </div>
                                <div class="mws-form-row bordered">
                                    <label class="mws-form-label">Gender</label>


                                    <?php
                                    $male = "";
                                    $female = "";
                                        if($user_info['user_gender'] == 1)
                                        {
                                            $male = "selected";
                                        }
                                        else
                                        {
                                            $female = "selected";
                                        }
                                    ?>
                                    <div class="mws-form-item">
                                        <select id="ugender" name="ugender" class="large">
                                            <option <?php echo $male; ?> value="1" >Male</option>
                                            <option <?php echo $female; ?> value="2" >Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="mws-form-row bordered">
                                    <label class="mws-form-label">Country of Passport</label>
                                    <div class="mws-form-item">
                                        <input id="ucountry" name="ucountry" type="text" value="<?php echo $user_info['user_country']; ?>" class="large">
                                    </div>
                                </div>
                                <div class="mws-button-row">
                                    <input type="submit" value="Save Member Details" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            <?php
            }
            else
            {
            ?>
            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-calendar"></i>Quick Member Add</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_member_add" class="mws-form" action="#">
                        <div class="mws-form-inline">
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Email</label>
                                <div class="mws-form-item">
                                    <input id="uemail" name="uemail" type="text" value="" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Name</label>
                                <div class="mws-form-item">
                                    <input id="ufname" name="ufname" type="text" value="" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Surname</label>
                                <div class="mws-form-item">
                                    <input id="usname" name="usname" type="text" value="" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Cell Number</label>
                                <div class="mws-form-item">
                                    <input id="ucelln" name="ucelln" type="text" value="" class="large">
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Gender</label>

                                <div class="mws-form-item">
                                    <select id="ugender" name="ugender" class="large">
                                        <option value="1" >Male</option>
                                        <option value="2" >Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mws-form-row bordered">
                                <label class="mws-form-label">Country of Passport</label>
                                <div class="mws-form-item">
                                    <input id="ucountry" name="ucountry" type="text" value="" class="large">
                                </div>
                            </div>
                            <div class="mws-button-row">
                                <input type="submit" value="Add Member" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php
            }
            ?>

            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Members Stats</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                        <thead>
                        <tr>
                            <th>Total Memebers</th>
                            <th>Export Users</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center"><?php echo "<h4>".count($users)."</h4>"; ?></td>
                            <td align="center">
                                    <a href="/admin/export_members" type="button" class="btn btn-danger"><i class="icon-cyclop"></i> Export All Users to CSV File</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

                <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Member Listing</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>Name</th>
                                <th>Email</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            if(count($users) > 0) {
                                foreach ($users as $item) {
                                    if($item['user_id'] != 1) {
                                        ?>
                                        <tr class="odd">
                                            <td class=" sorting_1"><?php echo $item['user_name']; ?></td>
                                            <td class=" "><?php echo $item['user_email']; ?></td>
                                            <td class=" ">
                                <span class="btn-group">
                                </span>
                                                <a id="lnk_edit_user" rel="tooltip" data-original-title="Edit Member Details"  href="#" data-user-id="<?php echo $item['user_id']; ?>"
                                                   class="btn_activity_view btn btn-small"><i
                                                        class="icon-eye-open"></i></a>
                                                <a rel="tooltip" data-original-title="Delete Member Details" id="lnk_delete_user" data-m-id="<?php echo $item['user_id']; ?>"
                                                   href="#" class="btn_activity_view btn btn-small"><i
                                                        class="icon-remove"></i></a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                }
                            }
                            else {
                            ?>
                            <tr>
                                <td colspan="4"> No Data Found</td>
                            </tr>

                            <?php
                            }
                            ?>
                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->


    </div>
