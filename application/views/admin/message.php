<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>

            <div id="mws_message_form_msg" class="mws-form-message success" style="display:none"></div>

            <div class="clear"></div>

            <div class="mws-panel grid_4 mws-collapsible">
            <div class="mws-panel-header">
                <span><i class="icon-calendar"></i>Send Ziwa Message</span>
            </div>
            <div class="mws-panel-body no-padding">
                <form id="frm_message_send" class="mws-form" action="#">
                    <div class="mws-form-block">

                        <?php
                        $parent_id = 0;
                        $user_to = "";
                        if(isset($message_details))
                        {
                            $parent_id = $message_details->update_id;
                            $user_to = $message_details->update_from;

                            $option = array("General Enquiry" => "General Enquiry",
                                            "Tour Enquiry" => "Tour Enquiry",
                                            "Package Enquiry" => "Package Enquiry",
                                            "Private Chauffeur Enquiry" => "Private Chauffeur Enquiry",
                                            "Exclusive Enquiry" => "Exclusive Enquiry",
                                            "Billing Enquiry" => "Billing Enquiry",
                                            "Other" => "Other");
                        }


                        ?>

                        <input id="message_parent" name="message_parent" type="hidden" value="<?php echo $parent_id; ?>" class="large" />

                        <div class="mws-form-row">
                            <label class="mws-form-label">Message Subject</label>
                            <div class="mws-form-item">
                                <select id="message_subject" name="message_subject" class="large">
                                    <?php
                                    foreach($option as $item)
                                    {
                                        echo "<option ".(($message_details->update_subject == $item) ? 'selected' : '')." value'$item' >$item</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

                        <div class="mws-form-row">
                            <label class="mws-form-label">To Email</label>
                            <div class="mws-form-item">
                                <input id="message_to" name="message_to" type="text" value="<?php echo $user_to; ?>" class="large" />
                            </div>
                        </div>

                        <div class="mws-form-row">
                            <label class="mws-form-label">Message</label>
                            <div class="mws-form-item">
                                    <textarea row="30" id="message_body" name="message_body" class="large">

                                    </textarea>
                            </div>
                        </div>

                        <div class="mws-button-row">
                            <input id="btn_message_add" type="submit" value="Add Tour" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
        </div>

            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Message Stats</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                        <thead>
                        <tr>
                            <th>Total Messages</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center"><?php echo "<h4>".count($messages)."</h4>"; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Message Activity</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>From</th>
                                <th>Subject</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            foreach($messages as $item) {
                            ?>
                                <tr class="odd">
                                    <td class=" sorting_1"><?php echo $item['update_from']; ?></td>
                                    <td class=" "><?php echo $item['update_subject']; ?></td>
                                    <td class=" ">
                                        <span class="btn-group">
                                        </span>
                                        <a  rel="popover" data-trigger="hover" data-placement="left" data-content="<?php echo $item['update_content']; ?>" data-original-title="<?php echo $item['update_subject']; ?>"  href="#" class="btn_activity_view btn btn-small"><i
                                                class="icon-eye-open"></i></a>
                                        <a rel="tooltip" data-placement="top" data-original-title="Reply Message" id="lnk_edit_message" href="/admin/message/<?php echo $item['update_id']; ?>" class="btn_activity_view btn btn-small"><i
                                                class="icon-pencil"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>

                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->

    </div>
