<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>

            <div id="mws_blog_form_msg" class="mws-form-message success" style="display:none"></div>


            <div class="clear"></div>

            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-calendar"></i>Add Ziwa Blog</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_blog_add" class="mws-form" action="#">
                        <div class="mws-form-block">


                            <div class="mws-form-row">
                                <label class="mws-form-label">Blog Name </label>
                                <div class="mws-form-item">
                                    <input id="blog_name" name="blog_name" type="text" value="" class="large" />
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Blog Article</label>
                                <div class="mws-form-item">
                                    <textarea row="30" id="blog_aticle" name="blog_aticle" class="large">

                                    </textarea>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Blog Summary</label>
                                <div class="mws-form-item">
                                    <textarea row="10" id="blog_summary" name="blog_summary" class="large">

                                    </textarea>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Blog Tags</label>
                                <div class="mws-form-item">
                                    <select data-placeholder="Choose Blog Tags..." multiple id="blog_tags" class="large">
                                        <option value="Tour">Tour</option>
                                        <option value="News">News</option>
                                        <option value="Stories">Stories</option>
                                        <option value="Package">Package</option>
                                        <option value="Special">Special</option>
                                        <option value="Exclusive">Exclusive</option>
                                        <option value="Competitions">Competitions</option>
                                        <option value="Blog">Blog</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>




                            <div class="mws-button-row">
                                <input id="btn_blog_add" type="submit" value="Add Blog" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Blog Stats</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                        <thead>
                        <tr>
                            <th>Total Blogs</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center"><?php echo "<h4>".count($blogs)."</h4>"; ?></td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mws-panel grid_4 mws-collapsible">

                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Blogs</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>Name</th>
                                <th>Actions</th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            if(count($blogs) > 0) {

                                foreach($blogs as $item) {
                                    ?>
                                    <tr class="odd">
                                        <td width="70%" class=" sorting_1"><?php echo $item['blog_title']; ?></td>
                                        <td class=" " width="30%">
                                        <span class="btn-group">
                                                    </span>
                                            <a rel="tooltip" data-placement="top" id="lnk_upload_main_image_blog" data-original-title="Add Blog Main Image" data-t-id="<?php echo $item['blog_id']; ?>" href="/admin/blog_mainUpload/<?php echo $item['blog_id']; ?>" class="btn_activity_view btn btn-small"><i
                                                    class="icon-camera"></i></a>
                                            <a rel="tooltip" data-placement="top" id="lnk_delete-blog" data-original-title="Delete Blog Post" data-t-id="<?php echo $item['blog_id']; ?>" href="#" class="btn_activity_view btn btn-small"><i
                                                    class="icon-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->


    </div>
