<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>

            <div id="mws_testimonial_form_msg" class="mws-form-message success" style="display:none"></div>


            <div class="clear"></div>

            <div class="mws-panel grid_4 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-calendar"></i>Add Customer Testimonials</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <form id="frm_testimonial" class="mws-form" action="#">
                        <div class="mws-form-inline">
                            <div class="mws-form-row">
                                <label class="mws-form-label">Date for Testimonial</label>
                                <div class="mws-form-item">
                                    <div class="large" id="testimonial_date_picker" name="testimonial_date_picker" ></div>
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Headline </label>
                                <div class="mws-form-item">
                                    <input id="headline" name="headline" type="text" value="" class="large" />
                                </div>
                            </div>

                            <div class="mws-form-row">
                                <label class="mws-form-label">Author</label>
                                <div class="mws-form-item">
                                    <input id="author" name="author" type="text" value="" class="large" />
                                </div>
                            </div>


                            <div class="mws-form-row">
                                <label class="mws-form-label">Testimonial Body</label>
                                <div class="mws-form-item">
                                    <textarea row="30" id="testimonial_body" name="testimonial_body" class="large">

                                    </textarea>
                                </div>
                            </div>

                            <div class="mws-button-row">
                                <input id="btn_testimonial_add" type="submit" value="Request Booking" class="btn btn-success">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <div class="mws-panel grid_4">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Testimonials Stats</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                        <thead>
                        <tr>
                            <th>Total Testimonials</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center"><?php echo "<h4>".count($testimonials)."</h4>"; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mws-panel grid_4 mws-collapsible">

                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Customer Testimonials</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>From</th>
                                <th>Date</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            if(count($testimonials) > 0) {

                                foreach($testimonials as $item) {

                                        ?>
                                        <tr class="odd">
                                            <td class=" sorting_1"><?php echo $item['t_author']; ?></td>
                                            <td class=" "><?php echo date('d-M-Y', strtotime($item['t_date'])); ?></td>
                                            <td class=" " width="30%">
                                        <span class="btn-group">
                                                    </span>
                                                <a rel="popover" data-trigger="hover" data-placement="left"
                                                   data-content="<?php echo $item['t_body']; ?>"
                                                   data-original-title="<?php echo $item['t_headline']; ?>" href="#"
                                                   class="btn_activity_view btn btn-small"><i
                                                        class="icon-eye-open"></i></a>
                                                <a rel="tooltip" data-original-title="Upload Image" id="lnk_upload_image_testimonial"
                                                   data-t-id="<?php echo $item['t_id']; ?>"
                                                   href="/admin/testimonialsUpload/<?php echo $item['t_id']; ?>"
                                                   class="btn_activity_view btn btn-small"><i
                                                        class="icon-camera-2"></i></a>
                                                <a rel="tooltip" data-original-title="Delete" id="lnk_delete_testimonial" data-t-id="<?php echo $item['t_id']; ?>"
                                                   href="#" class="btn_activity_view btn btn-small"><i
                                                        class="icon-remove"></i></a>
                                            </td>
                                        </tr>
                                    <?php

                                }
                            }
                            ?>
                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->


    </div>
