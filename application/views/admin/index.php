<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>assests/plugins/jgrowl/jquery.jgrowl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/members.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">

            <!-- Statistics Button Container -->
            <div class="mws-stat-container clearfix">


                <!-- Statistic Item -->
                <a class="mws-stat" href="#">
                    <!-- Statistic Icon (edit to change icon) -->
                    <span class="mws-stat-icon icol32-comments"></span>

                    <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">Enquiries</span>
                            <span class="mws-stat-value"><?php echo $update_totals; ?></span>
                        </span>
                </a>

                <?php
                //print_r($booking_totals[0]);
                $totals_array = array('priv'=>0,'pack'=>0,'tour'=>0,'excl'=>0);
                foreach($booking_totals as $booking_totals_item)
                {
                    if($booking_totals_item["booking_type_name"] == "Private Chauffeur")
                    {
                        $totals_array['priv'] = $booking_totals_item["type_count"];
                    }
                    else if($booking_totals_item["booking_type_name"] == "Package Deal")
                    {
                        $totals_array['pack'] = $booking_totals_item["type_count"];
                    }
                    else if($booking_totals_item["booking_type_name"] == "Tour")
                    {
                        $totals_array['tour'] = $booking_totals_item["type_count"];
                    }
                    else if($booking_totals_item["booking_type_name"] == "Exclusive Tour")
                    {
                        $totals_array['excl'] = $booking_totals_item["type_count"];
                    }
                    else
                    {

                    }
                }

                ?>

                <!-- Statistic Item -->
                <a class="mws-stat" href="#">
                    <!-- Statistic Icon (edit to change icon) -->
                    <span class="mws-stat-icon icol32-user-suit"></span>

                    <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">Private Chauffeur</span>
                            <span class="mws-stat-value"><?php echo $totals_array['priv']; ?></span>
                        </span>
                </a>

                <a class="mws-stat" href="#">
                    <!-- Statistic Icon (edit to change icon) -->
                    <span class="mws-stat-icon icol32-package"></span>

                    <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">Package Deals</span>
                            <span class="mws-stat-value "><?php echo $totals_array['pack']; ?></span>
                        </span>
                </a>

                <a class="mws-stat" href="#">
                    <!-- Statistic Icon (edit to change icon) -->
                    <span class="mws-stat-icon icol32-car"></span>

                    <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">Tours</span>
                            <span class="mws-stat-value"><?php echo $totals_array['tour']; ?></span>
                        </span>
                </a>

                <a class="mws-stat" href="#">
                    <!-- Statistic Icon (edit to change icon) -->
                    <span class="mws-stat-icon icol32-gift-add"></span>

                    <!-- Statistic Content -->
                        <span class="mws-stat-content">
                        	<span class="mws-stat-title">Exclusive</span>
                            <span class="mws-stat-value"><?php echo $totals_array['excl']; ?></span>
                        </span>
                </a>

            </div>

            <!-- Panels Start -->




            <div class="clear"></div>

            <div class="mws-panel grid_2">
                <div class="mws-panel-header">
                    <span><i class="icon-book"></i> Ziwa Booking Totals</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <ul class="mws-summary clearfix">

                        <?php
                        foreach($live_stats as $item) {
                        ?>
                            <li>
                                <span class="key"><?php echo $item['myformat']; ?></span>
                                <span class="val">
                                    <span class="text-nowrap"><?php echo $item['tot']; ?></span>
                                </span>
                            </li>
                        <?php
                        }
                        ?>

                    </ul>
                </div>
            </div>

            <div class="mws-panel grid_6 mws-collapsible">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Booking Activity</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>Activity</th>
                                <th>Status</th>
                                <th>Booking Date</th>
                                <th></th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">


                            <?php
                            if(count($booking_records) > 0) {

                                $statuses = [0=>'Booking Requested by You',1=>'Ziwa Responded to your Request',2=>'Awaiting Confirmation',3=>'Your Booking was Canceled',4=>'Your Booking was Confirmed'];
                                foreach($booking_records as $booking_record_item) {
                                    $url = "/admin/view_booking/".$booking_record_item['booking_transaction_id']."_".$booking_record_item['transaction_token'];
                                    ?>

                                    <tr class="odd">
                                        <td class=" sorting_1"><?php echo $booking_record_item['booking_type_name']; ?></td>
                                        <td class=" "><?php echo $statuses[$booking_record_item['status']] ?></td>
                                        <td class=" "><?php echo date('d-M-Y',strtotime($booking_record_item["date_booked_for"]));?></td>
                                        <td class=" ">
                                        <span class="btn-group">
                                        </span>
                                            <a href="<?php echo $url; ?>" class="btn_activity_view btn btn-small"><i
                                                    class="icon-eye-open"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            else
                            {
                                ?>
                                <tr>
                                    <td></td>
                                    <td class=" sorting_1" >No Data Found</td>
                                    <td></td>
                                </tr>

                            <?php
                            }
                            ?>



                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->

    </div>
