<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/plupload/plupload.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/plupload/plupload.flash.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/plupload/plupload.html4.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/plupload/plupload.html5.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/plupload/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/plupload/jquery.plupload.queue.css" />

<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>


<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>


            <div class="mws-panel grid_8">
                <div class="mws-panel-header">
                    <span><i class="icon-upload"></i>Tours Gallery File Uploader</span>
                </div>
                <div class="mws-panel-body no-padding">

                        <div id="tours_gal_uploader">
                            <p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
                        </div>
                </div>
            </div>
            <div class="mws-button-row">
                <center><a href="/admin/tours" class="btn btn-danger">Back top Tours</a></center>
            </div>

            <input id="t_id" name="t_id" type="hidden" value="<?php echo $t_id; ?>" />
            <div class="clear"></div>
         <!-- Panels End -->
        </div>
        <!-- Inner Container End -->

        <!-- Footer -->
        <div id="mws-footer">
            Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
        </div>

        </div>
        <!-- Main Container End -->


    </div>
