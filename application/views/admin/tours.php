<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>

<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        $this->load->view("includes/admin/adminnav");
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">
            <div class="clear"></div>

            <div id="mws_tours_form_msg" class="mws-form-message success" style="display:none"></div>


            <div class="clear"></div>
            <?php

            if(isset($tour->tour_id))
            {
            ?>

                <div class="mws-panel grid_3 mws-collapsible">
                    <div class="mws-panel-header">
                        <span><i class="icon-calendar"></i>Edit Ziwa Tour</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <form id="frm_tours_edit" class="mws-form" action="#">
                            <div class="mws-form-block">

                                <input id="tour_id" name="tour_id" type="hidden" value="<?php echo $tour->tour_id; ?>" class="large"/>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Name </label>

                                    <div class="mws-form-item">
                                        <input id="tour_name" name="tour_name" type="text" value="<?php echo urldecode($tour->tour_name); ?>" class="large"/>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Overview</label>

                                    <div class="mws-form-item">
                                    <textarea row="30" id="tour_overview" name="tour_overview" class="large">
                                        <?php echo urldecode($tour->tour_overview); ?>
                                    </textarea>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour About</label>

                                    <div class="mws-form-item">
                                    <textarea row="30" id="tour_about" name="tour_about" class="large">
                                        <?php echo urldecode($tour->tour_about); ?>
                                    </textarea>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Tags</label>

                                    <div class="mws-form-item">
                                        <select data-placeholder="Choose Tour Tags..." multiple id="tour_tags"
                                                class="large">
                                            <option <?php echo((substr_count($tour->tour_tags, 'Full Day') > 0) ? "selected" : ""); ?> value="Full Day">Full Day</option>
                                            <option <?php echo((substr_count($tour->tour_tags, 'Half Day') > 0) ? "selected" : ""); ?> value="Half Day">Half Day</option>
                                            <option <?php echo((substr_count($tour->tour_tags, 'Packaged') > 0) ? "selected" : ""); ?> value="Packaged">Packaged</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Types</label>

                                    <div class="mws-form-item">
                                        <ul class="mws-form-list inline">
                                            <li><input type="checkbox" id="tour_special" name="tour_special"  <?php echo(($tour->is_special == 1) ? "checked" : ""); ?> > <label
                                                    for="tos_y">Is Special</label></li>
                                            <li><input type="checkbox" id="tour_eclusive" name="tour_eclusive" <?php echo(($tour->is_exclusive == 1) ? "checked" : ""); ?> > <label
                                                    for="tos_y">Is Exclusive</label></li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="mws-button-row">
                                    <input id="btn_tours_edit" type="submit" value="Save Tour" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            <?php
            }
            else
            {
            ?>
                <div class="mws-panel grid_3 mws-collapsible">
                    <div class="mws-panel-header">
                        <span><i class="icon-calendar"></i>Add Ziwa Tour</span>
                    </div>
                    <div class="mws-panel-body no-padding">
                        <form id="frm_tours_add" class="mws-form" action="#">
                            <div class="mws-form-block">


                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Name </label>

                                    <div class="mws-form-item">
                                        <input id="tour_name" name="tour_name" type="text" value="" class="large"/>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Overview</label>

                                    <div class="mws-form-item">
                                    <textarea row="30" id="tour_overview" name="tour_overview" class="large">

                                    </textarea>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour About</label>

                                    <div class="mws-form-item">
                                    <textarea row="30" id="tour_about" name="tour_about" class="large">

                                    </textarea>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Tags</label>

                                    <div class="mws-form-item">
                                        <select data-placeholder="Choose Tour Tags..." multiple id="tour_tags"
                                                class="large">
                                            <option value="Full Day">Full Day</option>
                                            <option value="Half Day">Half Day</option>
                                            <option value="Packaged">Packaged</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="mws-form-row">
                                    <label class="mws-form-label">Tour Types</label>

                                    <div class="mws-form-item">
                                        <ul class="mws-form-list inline">
                                            <li><input type="checkbox" id="tour_special" name="tour_special"  > <label
                                                    for="tos_y">Is Special</label></li>
                                            <li><input type="checkbox" id="tour_eclusive" name="tour_eclusive"> <label
                                                    for="tos_y">Is Exclusive</label></li>
                                        </ul>
                                    </div>
                                </div>


                                <div class="mws-button-row">
                                    <input id="btn_tours_add" type="submit" value="Add Tour" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php
            }
            ?>

            <div class="mws-panel grid_5">
                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Tour Stats</span>
                </div>
                <div class="mws-panel-body no-padding">
                    <table class="mws-table">
                        <thead>
                        <tr>
                            <th>Total Tours</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td align="center"><?php echo "<h4>".count($tours)."</h4>"; ?></td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="mws-panel grid_5 mws-collapsible">

                <div class="mws-panel-header">
                    <span><i class="icon-table"></i>Ziwa Tours</span>
                    <div class="mws-collapse-button mws-inset"><span></span></div></div>
                <div class="mws-panel-inner-wrap"><div class="mws-panel-body no-padding">
                        <table id="tbl_members_activity" class="mws-table mws-datatable dataTable">
                            <thead>
                            <tr role="row">
                                <th>Name</th>
                                <th>Special</th>
                                <th>Exclusive</th>
                                <th>Actions</th>
                            </thead>

                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php
                            if(count($tours) > 0) {

                                foreach($tours as $item) {
                            ?>
                            <tr class="odd">
                                <td width="40%" class=" sorting_1"><?php echo urldecode($item['tour_name']); ?></td>
                                <td width="5%" class=" "><?php echo(($item['is_special'] == 1) ? "Yes" : "No"); ?></td>
                                <td width="5%"
                                    class=" "><?php echo(($item['is_exclusive'] == 1) ? "Yes" : "No"); ?></td>
                                <td class=" " width="50%">
                                        <span class="btn-group">
                                            <div class="btn-group" role="group" aria-label="...">
                                                    </span>
                                    <a rel="tooltip" data-placement="top" id="lnk_view_tours"
                                       data-original-title="Edit Tour" data-t-id="<?php echo $item['tour_id']; ?>"
                                       href="/admin/tours/edit/<?php echo $item['tour_id']; ?>" class="btn_activity_view btn btn-small"><i
                                            class="icon-eye-open"></i></a>
                                    <a rel="tooltip" data-placement="top" id="lnk_upload_main_image_tours"
                                       data-original-title="Add Tour Map Image"
                                       data-t-id="<?php echo $item['tour_id']; ?>"
                                       href="/admin/tours_mapUpload/<?php echo $item['tour_id']; ?>"
                                       class="btn_activity_view btn btn-small"><i
                                            class="icon-globe"></i></a>
                                    <a rel="tooltip" data-placement="top" id="lnk_upload_main_image_tours"
                                       data-original-title="Add Tour Main Image"
                                       data-t-id="<?php echo $item['tour_id']; ?>"
                                       href="/admin/tours_mainUpload/<?php echo $item['tour_id']; ?>"
                                       class="btn_activity_view btn btn-small"><i
                                            class="icon-camera"></i></a>
                                    <a rel="tooltip" data-placement="top" id="lnk_upload_images_tours"
                                       data-original-title="Add Tour Gallery Images"
                                       data-t-id="<?php echo $item['tour_id']; ?>"
                                       href="/admin/tours_galUpload/<?php echo $item['tour_id']; ?>"
                                       class="btn_activity_view btn btn-small"><i
                                            class="icon-camera-2"></i></a>

                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn_activity_view btn btn-small dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false">
                                            Settings
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php
                                            if($item['is_special'] == 1){
                                            ?>
                                                <li><a href="/admin/tours/special/<?php echo $item['tour_id']; ?>/0">Remove As Special</a></li>
                                            <?php
                                            }else{
                                            ?>
                                                <li><a href="/admin/tours/special/<?php echo $item['tour_id']; ?>/1">Set As Special</a></li>
                                            <?php
                                            }
                                            if($item['is_exclusive'] == 1){
                                            ?>
                                                <li><a href="/admin/tours/exclusive/<?php echo $item['tour_id']; ?>/0">Remove As Exclusive</a></li>
                                            <?php
                                            }else{
                                            ?>
                                                <li><a href="/admin/tours/exclusive/<?php echo $item['tour_id']; ?>/1">Set As Exclusive</a></li>
                                            <?php
                                            }
                                            ?>

                                            </ul>
                                            </div>

                                            <button rel="tooltip" data-original-title="Delete Tour" data-t-id="<?php echo $item['tour_id']; ?>"  href="#" class="btn_delete_tours btn_activity_view btn btn-small"><i class="icon-trash"></i></button>

                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody></table>
                    </div></div>

                <div class="clear"></div>


                <!-- Panels End -->
            </div>
            <!-- Inner Container End -->

            <!-- Footer -->
            <div id="mws-footer">
                Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
            </div>

        </div>
        <!-- Main Container End -->


    </div>
