<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/validate/jquery.validate-min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/adminmembers.js"></script>
<!-- Start Main Wrapper -->
<div id="mws-wrapper">

    <!-- Necessary markup, do not remove -->
    <div id="mws-sidebar-stitch"></div>
    <div id="mws-sidebar-bg"></div>

    <!-- Sidebar Wrapper -->
    <div id="mws-sidebar">

        <!-- Hidden Nav Collapse Button -->
        <div id="mws-nav-collapse">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <!-- Searchbox -->
        <div id="mws-searchbox" class="mws-inset">
            <form action="typography.html">
                <input type="text" class="mws-search-input" placeholder="Search...">
                <button type="submit" class="mws-search-submit"><i class="icon-search"></i></button>
            </form>
        </div>

        <!-- Main Navigation -->
        <?php
        if($user_role == 2)
        {
            $this->load->view("includes/admin/adminnav");
        }
        else {
            $this->load->view("includes/members/membernav");
        }
        ?>
    </div>

    <!-- Main Container Start -->
    <div id="mws-container" class="clearfix">

        <!-- Inner Container Start -->
        <div class="container">

            <div class="clear"></div>

            <div id="mws_booking_form_msg" class="mws-form-message success" style="display:none"></div>

            <div class="clear"></div>

            <div class="mws-panel grid_8">
                <div class="mws-panel-header">
                    <span><i class="icon-book"></i>Ziwa Tours Booking Details</span>
                </div>

                <div class="mws-panel-body no-padding">
                    <ul class="mws-summary clearfix">
                        <li>
                            <span class="key">Booking Ref</span>
                                <span class="val">
                                    <span class="text-nowrap"><?php echo $booking_info->booking_type_id."_".$booking_info->transaction_token; ?></span>
                                </span>
                        </li>
                        <li>
                            <span class="key">Booking Type</span>
                                <span class="val">
                                    <span class="text-nowrap"><?php echo $book_type; ?></span>
                                </span>
                        </li>
                        <li>
                            <span class="key">Name</span>
                                <span class="val">
                                    <span class="text-nowrap">
                                        <?php
                                        if(isset($booking_name->bname)) {
                                            echo $booking_name->bname;
                                        }else{
                                            echo "Not Set";
                                        }
                                        ?></span>
                                </span>
                        </li>
                        <li>
                            <span class="key">Number of Adults</span>
                                <span class="val">
                                    <span class="text-nowrap">
                                        <input id="num_of_adults" type="text" value="<?php echo $booking_info->adults; ?>" />
                                    </span>
                                </span>
                        </li>
                        <li>
                            <span class="key">Number of Children</span>
                                <span class="val">
                                    <span class="text-nowrap">
                                        <input id="num_of_children" type="text" value="<?php echo $booking_info->children; ?>" />
                                    </span>
                                </span>
                        </li>
                        <li>
                            <span class="key">Booking Date</span>
                                <span class="val">
                                    <span class="text-nowrap">
                                         <input id="booking_date_for" type="date" value="<?php echo date('Y-m-d',strtotime($booking_info->date_booked_for)); ?>" /></span>
                                </span>
                        </li>
                        <?php
                        $booking_status = [0=>'Booking Requested by You',1=>'Ziwa Responded to your Request',2=>'Awaiting Confirmation',3=>'Your Booking was Canceled',4=>'Your Booking was Confirmed'];
                        ?>
                        <li>
                            <span class="key">Booking Status</span>
                                <span class="val">
                                    <span class="text-nowrap">
                                        <select id="booking_status_select">
                                            <?php
                                            $booking_index_count = 0;
                                            foreach($booking_status as $item)
                                            {
                                                $set = "";
                                                if($item == $booking_status[$booking_info->status])
                                                {
                                                    $set = "selected";
                                                }
                                            ?>
                                            <option <?php echo $set; ?> value="<?php echo $booking_index_count; ?>"><?php echo $item; ?></option>
                                            <?php
                                                $booking_index_count++;
                                            }
                                            ?>
                                        </select>
                                    </span>
                                </span>
                        </li>
                    </ul>

                    <div class="btn-toolbar" style="padding-left:10px;">
                        <center>
                        <div class="btn-group">
                            <a id="booking_save_details_button" href="#" data-book-id="<?php echo $booking_info->booking_type_id; ?>" class="btn"><i class="icol-page-save"></i>Update & Save Booking Record</a>
                            <?php
                            echo "booking info : ".$booking_info->status;

                            if($booking_info->status < 3) {
                            ?>
                                <a id="btn_accept_booking" data-book-id="<?php echo $booking_info->booking_type_id; ?>"
                                   href="#" class="btn"><i class="icol-accept"></i> Accept Booking Information</a>
                                <a id="btn_cancel_booking" data-book-id="<?php echo $booking_info->booking_type_id; ?>"
                                   href="#" class="btn"><i class="icol-cross"></i> Cancel Booking Request</a>
                            <?php
                            }
                            ?>
                            <a href="/admin/bookings" class="btn"><i class="icol-application"></i> Back to Booking List</a>
                        </div>
                        </center>
                    </div>
                </div>
            </div>


            <div class="clear"></div>


            <!-- Panels End -->
        </div>
        <!-- Inner Container End -->

        <!-- Footer -->
        <div id="mws-footer">
            Copyright Ziwa Tours <?php echo date('Y');?>. All Rights Reserved.
        </div>

    </div>
    <!-- Main Container End -->

</div>
