<?php

if ($layout === 'auth')
{
	$path_header = 'includes/auth/header';
	$path_footer = 'includes/auth/footer';
}
else if($layout === 'admin')
{
    $path_header = 'includes/admin/header';
    $path_footer = 'includes/admin/footer';
}
else if($layout === 'members')
{
    $path_header = 'includes/members/header';
    $path_footer = 'includes/members/footer';
}
else
{
    die("n error occured please contact Website Administrator");
}

$this->load->view($path_header);
$this->load->view($main_content);
$this->load->view($path_footer);