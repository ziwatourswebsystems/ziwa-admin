<div id="mws-navigation">
    <ul>
        <li><a href="<?php echo base_url(); ?>admin/"><i class="icon-home"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url(); ?>admin/bookings"><i class="icon-calendar"></i>Bookings</a></li>
        <li><a href="<?php echo base_url(); ?>admin/members"><i class="icon-official"></i>Members</a></li>
        <li><a href="<?php echo base_url(); ?>admin/testimonials"><i class="icon-bullhorn"></i>Testimonials</a></li>
        <li><a href="<?php echo base_url(); ?>admin/services"><i class="icon-th"></i> Services Page</a></li>
        <li><a href="<?php echo base_url(); ?>admin/tours"><i class="icon-th"></i> Tours Page</a></li>
        <li><a href="<?php echo base_url(); ?>admin/blog"><i class="icon-th"></i> Blog Page</a></li>
        <li><a href="<?php echo base_url(); ?>admin/message"><i class="icon-comments"></i>Message</a></li>
    </ul>
</div>