
<!-- JavaScript Plugins -->
<script src="<?php echo base_url(); ?>assets/js/libs/jquery-1.8.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/jquery.mousewheel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/jquery.placeholder.min.js"></script>
<script src="<?php echo base_url(); ?>assets/custom-plugins/fileinput.js"></script>

<!-- jQuery-UI Dependent Scripts -->
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-1.9.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url(); ?>assets/jui/js/timepicker/jquery-ui-timepicker.min.js"></script>

<!-- Plugin Scripts -->
<script src="<?php echo base_url(); ?>assets/plugins/imgareaselect/jquery.imgareaselect.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jgrowl/jquery.jgrowl-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/validate/jquery.validate-min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/colorpicker/colorpicker-min.js"></script>

<!-- Core Script -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core/mws.js"></script>

<!-- Themer Script (Remove if not needed) -->
<script src="<?php echo base_url(); ?>assets/js/core/themer.js"></script>

<!-- Demo Scripts (remove if not needed) -->
<script src="<?php echo base_url(); ?>assets/js/demo/demo.widget.js"></script>

</body>
</html>
