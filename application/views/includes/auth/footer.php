<!-- JavaScript Plugins -->
<script src="<?php echo base_url(); ?>assets/js/libs/jquery-1.8.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/libs/jquery.placeholder.min.js"></script>
<script src="<?php echo base_url(); ?>assets/custom-plugins/fileinput.js"></script>
<!-- jQuery-UI Dependent Scripts -->
<script src="<?php echo base_url(); ?>assets/jui/js/jquery-ui-effects.min.js"></script>
<!-- Plugin Scripts -->
<script src="<?php echo base_url(); ?>assets/plugins/validate/jquery.validate-min.js"></script>
<!-- Login Script -->
<script src="<?php echo base_url(); ?>assets/js/core/login.js"></script>

</body>
</html>
