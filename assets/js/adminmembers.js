(function( $, window, document, undefined ) {

    $(document).ready(function() {

        if($("#blog_tags").length) {
            $("#blog_tags").chosen();
        }

        if($("#tour_tags").length) {
            $("#tour_tags").chosen();
        }



        $("#booking_save_details_button").click(function(evt){
            evt.preventDefault();
            var booking_id = $(this).data("book-id");
            var num_of_adults = $("#num_of_adults").val();
            var num_of_children = $("#num_of_children").val();
            var booking_date = $("#booking_date_for").val();
            var booking_status = $("#booking_status_select").val();
            var form_data = [];
            form_data.push({ name: "booking_id", value: booking_id});
            form_data.push({ name: "num_of_adults", value: num_of_adults});
            form_data.push({ name: "num_of_children", value: num_of_children});
            form_data.push({ name: "booking_date", value: booking_date});
            form_data.push({ name: "booking_status", value: booking_status});

            $.post('/admin/updateBooking', form_data, function (data) {
                $("#mws_booking_form_msg").removeClass("error").addClass('success').html("Ziwa Booking Saved and Updated successfully.").show();
                setTimeout(function(){ window.location = "/admin/bookings"; }, 1500);
            }).fail(function () {
                $("#mws_booking_form_msg").removeClass("success").addClass('error').html("Ziwa Booking not Saved and Updated error were encountered if problem persists please email info@ziwatours.biz.").show();
            });

        });

        $("#btn_accept_booking").click(function(evt){
            evt.preventDefault();
            var booking_id = $(this).data("book-id");
            var form_data = [];
            form_data.push({ name: "booking_id", value: booking_id});
            $.post('/admin/acceptBooking', form_data, function (data) {
                $("#mws_booking_form_msg").removeClass("error").addClass('success').html("Ziwa Booking Accepted successfully.").show();
                setTimeout(function(){ window.location = "/admin/bookings"; }, 1500);
            }).fail(function () {
                $("#mws_booking_form_msg").removeClass("success").addClass('error').html("Ziwa Booking Accepted error were encountered if problem persists please email info@ziwatours.biz.").show();
            });
        });

        $("#btn_cancel_booking").click(function(evt){
            evt.preventDefault();
            var booking_id = $(this).data("book-id");
            var form_data = [];
            form_data.push({ name: "booking_id", value: booking_id});
            $.post('/admin/cancelBooking', form_data, function (data) {
                $("#mws_booking_form_msg").removeClass("error").addClass('success').html("SUCCESS : Ziwa Booking Cenceled sucessfully.").show();
                setTimeout(function(){ window.location = "/admin/bookings"; }, 1500);
            }).fail(function () {
                $("#mws_booking_form_msg").removeClass("success").addClass('error').html("ERROR : Ziwa Booking Cencel error were encountered if problem persists please email info@ziwatours.biz..").show();
            });
        });

        if($("#blog_main_uploader").length){

            $("#blog_main_uploader").pluploadQueue({
                // General settings
                runtimes: 'html4, html5',
                url: '/admin/doBlogMainUpload/' + $("#t_id").val(),
                max_file_size: '1000mb',
                max_file_count: 1, // user can add no more then 20 files at a time
                chunk_size: '1mb',
                unique_names: true,
                multiple_queues: true,
                // Resize images on clientside if we can
                resize: {width: 320, height: 240, quality: 90},
                // Rename files by clicking on their titles
                rename: true,
                // Sort files
                sortable: true,
                // Specify what files to browse for
                filters: [
                    {title: "Image files", extensions: "jpg,gif,png"},
                ]
            });

        }

        if($("#tours_map_uploader").length){

            $("#tours_map_uploader").pluploadQueue({
                // General settings
                runtimes: 'html4, html5',
                url: '/admin/doToursMapUpload/' + $("#t_id").val(),
                max_file_size: '1000mb',
                max_file_count: 1, // user can add no more then 20 files at a time
                chunk_size: '1mb',
                unique_names: true,
                multiple_queues: true,
                // Resize images on clientside if we can
                resize: {width: 320, height: 240, quality: 90},
                // Rename files by clicking on their titles
                rename: true,
                // Sort files
                sortable: true,
                // Specify what files to browse for
                filters: [
                    {title: "Image files", extensions: "jpg,gif,png"},
                ]
            });

        }

        if($("#tours_gal_uploader").length){
            $("#tours_gal_uploader").pluploadQueue({
                // General settings
                runtimes: 'html4, html5',
                url: '/admin/doToursGalUpload/' + $("#t_id").val(),
                max_file_size: '1000mb',
                max_file_count: 10, // user can add no more then 20 files at a time
                chunk_size: '1mb',
                unique_names: true,
                multiple_queues: true,
                // Resize images on clientside if we can
                resize: {width: 320, height: 240, quality: 90},
                // Rename files by clicking on their titles
                rename: true,
                // Sort files
                sortable: true,
                // Specify what files to browse for
                filters: [
                    {title: "Image files", extensions: "jpg,gif,png"},
                ]
            });

        }

        if($("#tours_main_uploader").length){

            $("#tours_main_uploader").pluploadQueue({
                // General settings
                runtimes: 'html4, html5',
                url: '/admin/doToursMainUpload/' + $("#t_id").val(),
                max_file_size: '1000mb',
                max_file_count: 1, // user can add no more then 20 files at a time
                chunk_size: '1mb',
                unique_names: true,
                multiple_queues: true,
                // Resize images on clientside if we can
                resize: {width: 320, height: 240, quality: 90},
                // Rename files by clicking on their titles
                rename: true,
                // Sort files
                sortable: true,
                // Specify what files to browse for
                filters: [
                    {title: "Image files", extensions: "jpg,gif,png"},
                ]
            });

        }

        // minDate: 'today',
        if($("#uploader").length) {

            $("#uploader").pluploadQueue({
                // General settings
                runtimes: 'html4, html5',
                url: '/admin/doTestimonialUpload/' + $("#t_id").val(),
                max_file_size: '1000mb',
                max_file_count: 1, // user can add no more then 20 files at a time
                chunk_size: '1mb',
                unique_names: true,
                multiple_queues: true,
                // Resize images on clientside if we can
                resize: {width: 320, height: 240, quality: 90},
                // Rename files by clicking on their titles
                rename: true,
                // Sort files
                sortable: true,
                // Specify what files to browse for
                filters: [
                    {title: "Image files", extensions: "jpg,gif,png"},
                ]
            });
        }

        if($("#testimonial_date_picker").length) {
            $("#testimonial_date_picker").datepicker({
                autoSize: true,
                changeMonth: true,
                numberOfMonths: 1,
                showOtherMonths: true,
                changeYear: true,
                numberOfYears: 1,
                showOtherYears: true
            });
        }


        $("#frm_message_send").validate({
            rules: {
                message_to:{
                    required: true,
                    email: true
                },
                message_body:{
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                message_to:{
                    required: "Please enter your Message",
                    email: "Message to must be a valid email address"
                },
                message_body:{
                    required: "Please provide a Message body",
                    minlength: "Message body must be at least 10 characters long"
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_message_send").serializeArray();

                console.log(form_data);

                $.post('/admin/addmessage', form_data, function (data) {
                    $("#mws_message_form_msg").removeClass("error").addClass('success').html("Ziwa Message send successfully.").show();
                    setTimeout(function(){ window.location = "/admin/message"; }, 1500);
                }).fail(function () {
                    $("#mws_message_form_msg").removeClass("success").addClass('error').html("Error sending Message :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });


            }

        });

        $("#frm_blog_add").validate({
            rules: {
                blog_name:{
                    required: true,
                    minlength: 4
                },
                blog_aticle:{
                    required: true,
                    minlength: 10
                },
                blog_summary:{
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                blog_name:{
                    required: "Please enter your Blog Title",
                    minlength: "Blog Title must be at least 4 characters long"
                },
                blog_aticle:{
                    required: "Please provide a Blog Article",
                    minlength: "Blog Article must be at least 10 characters long"
                },
                blog_summary:{
                    required: "Please provide a Blog Summary",
                    minlength: "Blog Summary must be at least 10 characters long"
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_blog_add").serializeArray();
                form_data.push({ name: "blog_tags", value: $("#blog_tags").val()});

                console.log(form_data);

                $.post('/admin/addblog', form_data, function (data) {
                    $("#mws_blog_form_msg").removeClass("error").addClass('success').html("Ziwa Blog Added successfully.").show();
                    setTimeout(function(){ window.location = "/admin/blog"; }, 1500);
                }).fail(function () {
                    $("#mws_blog_form_msg").removeClass("success").addClass('error').html("Error Adding Blog :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });


            }

        });

        $("#frm_tours_add").validate({
            rules: {
                tour_name:{
                    required: true,
                    minlength: 4
                },
                tour_overview: {
                    required: true,
                    minlength: 10
                },
                tour_about: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                tour_name: {
                    required: "Please enter your Tour name",
                    minlength: "Tour name must be at least 4 characters long"
                },
                tour_overview:{
                    required: "Please provide a tour voerview",
                    minlength: "tour_overview must be at least 10 characters long"
                },
                tour_about:{
                    required: "Please provide a tour voerview",
                    minlength: "tour_overview must be at least 10 characters long"
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_tours_add").serializeArray();
                form_data.push({ name: "tour_special", value: $('#tour_special:checked').length});
                form_data.push({ name: "tour_exclusive", value: $('#tour_eclusive:checked').length});
                form_data.push({ name: "tour_tags", value: $("#tour_tags").val()});
                console.log(form_data);

                $.post('/admin/addtour', form_data, function (data) {
                    $("#mws_tours_form_msg").removeClass("error").addClass('success').html("Ziwa Tour Added successfully.").show();
                    setTimeout(function(){ window.location = "/admin/tours"; }, 1500);
                }).fail(function () {
                    $("#mws_tours_form_msg").removeClass("success").addClass('error').html("Error Adding Ziwa Tour :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });


            }

        });


        $("#frm_tours_edit").validate({
            rules: {
                tour_name:{
                    required: true,
                    minlength: 4
                },
                tour_overview: {
                    required: true,
                    minlength: 10
                },
                tour_about: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                tour_name: {
                    required: "Please enter your Tour name",
                    minlength: "Tour name must be at least 4 characters long"
                },
                tour_overview:{
                    required: "Please provide a tour voerview",
                    minlength: "tour_overview must be at least 10 characters long"
                },
                tour_about:{
                    required: "Please provide a tour voerview",
                    minlength: "tour_overview must be at least 10 characters long"
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_tours_edit").serializeArray();
                form_data.push({ name: "tour_special", value: $('#tour_special:checked').length});
                form_data.push({ name: "tour_exclusive", value: $('#tour_eclusive:checked').length});
                form_data.push({ name: "tour_tags", value: $("#tour_tags").val()});

                $.post('/admin/edittour', form_data, function (data) {
                    $("#mws_tours_form_msg").removeClass("error").addClass('success').html("Ziwa Tour Saved successfully.").show();
                    setTimeout(function(){ window.location = "/admin/tours"; }, 1500);
                }).fail(function () {
                    console.log(form_data);
                    $("#mws_tours_form_msg").removeClass("success").addClass('error').html("Error Saving Ziwa Tour :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });


            }

        });


        $(".btn_delete_tours").click(function(evt){

            evt.preventDefault();
            var m_id = $(this).data("t-id");
            var form_data = [];
            form_data.push({ name: "t-id", value: m_id});
            $.post('/admin/deleteTour', form_data, function (data) {
                $("#mws_tours_form_msg").removeClass("error").addClass('success').html("Ziwa Tour Deleted successfully.").show();
                setTimeout(function(){ window.location = "/admin/tours"; }, 1500);
            }).fail(function () {
                $("#mws_tours_form_msg").removeClass("success").addClass('error').html("Ziwa Tour not Deleted error were encountered if problem persists please email info@ziwatours.biz.").show();
            });

        });

        $("#frm_member_add").validate({
            rules: {
                uemail:{
                    required: true,
                    email: true
                },
                ufname: {
                    required: true,
                    minlength: 4
                }
            },
            messages: {
                uemail: {
                    required: "Please enter your members email",
                    email: "Members email must be a valid email address"
                },
                ufname:{
                    required: "Please provide a member first name",
                    minlength: "Member first name must be at least 4 characters long"
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_member_add").serializeArray();
                console.log(form_data);
                $.post('/admin/addmember', form_data, function (data) {
                    $("#mws_member_form_msg").removeClass("error").addClass('success').html("Ziwa Member Added successfully.").show();
                   setTimeout(function(){ window.location = "/admin/members"; }, 1500);
                }).fail(function () {
                    $("#mws_member_form_msg").removeClass("success").addClass('error').html("Error Adding Ziwa Member :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });
            }

        });

        $("#frm_member_update").validate({
            rules: {
                uemail:{
                    required: true,
                    email: true
                },
                ufname: {
                    required: true,
                    minlength: 4
                }
            },
            messages: {
                uemail: {
                    required: "Please enter your members email",
                    email: "Members email must be a valid email address"
                },
                ufname:{
                    required: "Please provide a member first name",
                    minlength: "Member first name must be at least 4 characters long"
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_member_update").serializeArray();
                console.log(form_data);

                $.post('/admin/updatemember', form_data, function (data) {
                    $("#mws_member_form_msg").removeClass("error").addClass('success').html("Ziwa Member Updated successfully.").show();
                    setTimeout(function(){ window.location = "/admin/members"; }, 1500);
                }).fail(function () {
                    $("#mws_member_form_msg").removeClass("success").addClass('error').html("Error Updating Ziwa Member :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });

            }

        });

        $("#lnk_edit_user").click(function(evt){
            evt.preventDefault();
            var user_id = $(this).data("user-id");
            window.location = "/admin/members/edit/"+user_id;
        });



        $("#lnk_delete-blog").click(function(evt){
            evt.preventDefault();
            var m_id = $(this).data("t-id");
            var form_data = [];
            form_data.push({ name: "t-id", value: m_id});

            $.post('/admin/deleteBlog', form_data, function (data) {
                $("#mws_blog_form_msg").removeClass("error").addClass('success').html("Ziwa Blog Deleted successfully.").show();
                setTimeout(function(){ window.location = "/admin/blog"; }, 1500);
            }).fail(function () {
                $("#mws_blog_form_msg").removeClass("success").addClass('error').html("Ziwa Blog not Deleted error were encountered if problem persists please email info@ziwatours.biz.").show();
            });
        });


        $("#lnk_delete_user").click(function(evt){
            evt.preventDefault();
            var m_id = $(this).data("m-id");
            var form_data = [];
            form_data.push({ name: "m-id", value: m_id});

            $.post('/admin/deleteMember', form_data, function (data) {
                $("#mws_member_form_msg").removeClass("error").addClass('success').html("Ziwa Member Deleted successfully.").show();
                setTimeout(function(){ window.location = "/admin/members"; }, 1500);
            }).fail(function () {
                $("#mws_member_form_msg").removeClass("success").addClass('error').html("Ziwa Member not Deleted error were encountered if problem persists please email info@ziwatours.biz.").show();
            });
        });


        $("#frm_services").validate({
            rules: {
                servicename:{
                    required: true,
                    minlength: 4
                },
                service_body: {
                    required: true,
                    minlength: 4,
                }
            },
            messages: {
                servicename: {
                    required: "Please enter your service name",
                    minlength: "Your service name must be at least 4 characters long"
                },
                service_body:{
                    required: "Please provide a service body",
                    minlength: "Your service body  must be at least 4 characters long",
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_services").serializeArray();
                console.log(form_data);
                $.post('/admin/addservice', form_data, function (data) {
                    $("#mws_services_form_msg").removeClass("error").addClass('success').html("Ziwa Service Added successfully.").show();
                    setTimeout(function(){ window.location = "/admin/services"; }, 1500);
                }).fail(function () {
                    $("#mws_services_form_msg").removeClass("success").addClass('error').html("Error Adding Ziwa Service :  error were encountered if problem persists please email info@ziwatours.biz.").show();
                });
            }

        });

        $("#lnk_delete_service").click(function(evt){
            evt.preventDefault();
            var s_id = $(this).data("s-id");
            var form_data = [];
            form_data.push({ name: "s-id", value: s_id});

            $.post('/admin/deleteService', form_data, function (data) {
                $("#mws_services_form_msg").removeClass("error").addClass('success').html("Ziwa Service Deleted successfully.").show();
                setTimeout(function(){ window.location = "/admin/services"; }, 1500);
            }).fail(function () {
                $("#mws_services_form_msg").removeClass("success").addClass('error').html("Ziwa Service not Deleted error were encountered if problem persists please email info@ziwatours.biz.").show();
            });
        });


        $("#frm_testimonial").validate({
            rules: {
                headline:{
                    required: true,
                    minlength: 4
                },
                author:{
                    required: true,
                    minlength: 4
                },
                testimonial_body: {
                    required: true,
                    minlength: 4,
                }
            },
            messages: {
                headline: {
                    required: "Please enter your headline",
                    minlength: "Your headline must be at least 4 characters long"
                },
                author: {
                    required: "Please enter your author and location details",
                    minlength: "Your author and location details must be at least 4 characters long"
                },
                testimonial_body:{
                    required: "Please provide a testimonial",
                    minlength: "Your testimonial  must be at least 4 characters long",
                }
            },
            submitHandler: function(form) {
                var form_data = [];
                form_data = $("#frm_testimonial").serializeArray();
                form_data.push({ name: "testimonial_date", value: $("#testimonial_date_picker").val()});

                $.post('/admin/addTestimonial', form_data, function (data) {
                    $("#mws_testimonial_form_msg").removeClass("error").addClass('success').html("Ziwa Testimonial Added successfully.").show();
                    setTimeout(function(){ window.location = "/admin/testimonials"; }, 1500);
                }).fail(function () {
                    $("#mws_testimonial_form_msg").removeClass("success").addClass('error').html("Ziwa Testimonial Not Added error were encountered if problem persists please email info@ziwatours.biz.").show();
                });
            }

        });

        $("#lnk_delete_testimonial").click(function(evt){
            evt.preventDefault();
            var t_id = $(this).data("t-id");
            var form_data = [];
            form_data.push({ name: "t-id", value: t_id});

            $.post('/admin/deleteTestiumonial', form_data, function (data) {
                $("#mws_testimonial_form_msg").removeClass("error").addClass('success').html("Ziwa Testimonial Added successfully.").show();
                setTimeout(function(){ window.location = "/admin/testimonials"; }, 1500);
            }).fail(function () {
                $("#mws_testimonial_form_msg").removeClass("success").addClass('error').html("Ziwa Testimonial Not Added error were encountered if problem persists please email info@ziwatours.biz.").show();
            });
        });

        if ($('#tbl_members_activity').length) {
            $('#tbl_members_activity').dataTable();
        }



        $("#btn_member_enquiry").click(function(evt){
            evt.preventDefault();
            var form_data = $("#frm_member_enquiry").serializeArray();


            if ($.trim($("#enquiry_body").val())) {
                $.post('/members/sendMessage', form_data, function (data) {
                    if (data != "succes") {
                        $("#mws_enquiry_form_msg").removeClass("error").addClass('success').html("Ziwa Message was sent successfully.");
                        $("#mws_enquiry_form_msg").show();
                        $('#frm_member_enquiry').trigger("reset");
                    }
                    else {
                        $("#mws_enquiry_form_msg").removeClass("success").addClass('error').html("Data Error Ziwa Message was not sent successfully if problem persists please email info@ziwatours.biz.");
                        $("#mws_enquiry_form_msg").show();
                    }
                }).fail(function () {
                    $("#mws_enquiry_form_msg").removeClass("success").addClass('error').html("Coonection Error Ziwa Message was not sent successfully if problem persists please email info@ziwatours.biz.");
                    $("#mws_enquiry_form_msg").show();
                });
            }
            else
            {
                $("#mws_enquiry_form_msg").removeClass("success").addClass('error').html("Validation Error Ziwa Message was not sent successfully if problem persists please email info@ziwatours.biz.");
                $("#mws_enquiry_form_msg").show();

            }
        });

        $("#memeber_save_profile").click(function(evt){
            evt.preventDefault();
            var form_data = $("#frm_user_profile").serializeArray();
            //console.log(form_data);
            $.post('/members/saveUserProfile', form_data, function(data){
                if(data != "succes") {
                    $("#user_profile_mws-validate-error").removeClass("error").addClass('success').html("Profile information was saved successfully.");
                    $("#user_profile_mws-validate-error").show();
                }
                else {
                    $("#user_profile_mws-validate-error").removeClass("success").addClass('error').html("Data Error Profile information was not saved successfully if problem persists please email info@ziwatours.biz.");
                    $("#user_profile_mws-validate-error").show();
                }
            }).fail(function() {
                $("#user_profile_mws-validate-error").removeClass("success").addClass('error').html("Coonection Error Profile information was not saved successfully if problem persists please email info@ziwatours.biz.");
                $("#user_profile_mws-validate-error").show();
            });
        });

        $("#memeber_save_password").click(function(evt){
            evt.preventDefault();
            if($("#opass").val().trim().length < 3) {
                $("#mws-password-validate-error").removeClass("success").addClass('error').html("Please enter a password before trying to save your password.");
                $("#mws-password-validate-error").show();
            }
            else
            {
                var form_data = [];
                form_data.push({ name: "use_id", value: $("#password_user_id").val()});
                form_data.push({ name: "use_password", value: $("#opass").val().trim()});
                $.post('/members/changeUserPassword', form_data, function(data){
                    if(data != "succes") {
                        $("#mws-password-validate-error").removeClass("error").addClass('success').html("Password was saved successfully.");
                        $("#mws-password-validate-error").show();
                        $('#frm_user_password').trigger("reset");
                    }
                    else {
                        $("#mws-password-validate-error").removeClass("success").addClass('error').html("Data Error password was not saved successfully if problem persists please email info@ziwatours.biz.");
                        $("#mws-password-validate-error").show();
                    }
                }).fail(function() {
                    $("#mws-password-validate-error").removeClass("success").addClass('error').html("Coonection Error password was not saved successfully if problem persists please email info@ziwatours.biz.");
                    $("#mws-password-validate-error").show();
                });

            }

        });



    });


}) (jQuery, window, document);