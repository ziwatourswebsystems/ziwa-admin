/*
 * MWS Admin v2.1 - Login JS
 * This file is part of MWS Admin, an Admin template build for sale at ThemeForest.
 * All copyright to this file is hold by Mairel Theafila <maimairel@yahoo.com> a.k.a nagaemas on ThemeForest.
 * Last Updated:
 * December 08, 2012
 *
 */
(function($) {
	$(document).ready(function() {	
		$("#mws-login-form form").validate({
			rules: {
				username: {required: true}, 
				password: {required: true}
			}, 
			errorPlacement: function(error, element) {  
			}, 
			invalidHandler: function(form, validator) {
				if($.fn.effect) {
					$("#mws-login").effect("shake", {distance: 6, times: 2}, 35);
				}
			}
            /*,
            submitHandler: function(form) {
                var ziwa_frm_data = [];
                ziwa_frm_data.push({name: "user_name", value: $("#username").val()});
                ziwa_frm_data.push({name: "user_password", value: $("#user_password").val()});
                $.post('auth/check_creds', ziwa_frm_data, function(data){

                    if(data != 0) {
                        console.log("success");
                    }else {
                        console.log("process error something went wrong");
                    }

                }).fail(function() {
                    console.log("process failed");
                });
                return false;

            }*/
		});

		$.fn.placeholder && $('[placeholder]').placeholder();
	});
}) (jQuery);
