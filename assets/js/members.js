(function( $, window, document, undefined ) {

    $(document).ready(function() {

        if ($('#tbl_members_activity').length) {
            $('#tbl_members_activity').dataTable();
        }

        function HideServices()
        {
            $("#panel_service_1").hide();
            $("#panel_service_2").hide();
            $("#panel_service_3").hide();
            $("#panel_service_4").hide();
        }

        $("#service_selector").change(function() {
            var seleted_service_id = $(this).val();
            //hide them all
            HideServices();
            $("#panel_service_"+seleted_service_id).show();
        });

        $("#member_add_booking_btn").click(function(evt){
            evt.preventDefault();
            var date_from = $("#date_from").val();
            var selected_service_id = $("#service_selector").val();
            var sub_selected_id = $("#select_service_"+selected_service_id).val();
            var adults = $("#adults_sel").val();
            var children =  $("#children_sel").val();


            var form_data = [];
            form_data.push({ name: "booking_date", value: date_from});
            form_data.push({ name: "booking_type_id", value: selected_service_id});
            form_data.push({ name: "booking_id", value: sub_selected_id });
            form_data.push({ name: "booking_adults", value: adults});
            form_data.push({ name: "booking_children", value: children});

            $.post('/members/addBooking', form_data, function (data) {
                // show success msg and reload
                $("#add_booking_msg").removeClass("error").addClass('success').html("Booking Reservation Added").show();
                setTimeout(function(){ window.location = "/members/"; }, 1500);

            }).fail(function () {
                // show error msg
                $("#add_booking_msg").removeClass("success").addClass('error').html("Booking Reservation Error").show();

            });

        });


        $("#btn_accept_booking").click(function(evt){
            evt.preventDefault();
            var booking_id = $(this).data("book-id");
            var form_data = [];
            form_data.push({ name: "booking_id", value: booking_id});
            $.post('/members/acceptBooking', form_data, function (data) {
                $("#mws_booking_form_msg").removeClass("error").addClass('success').html("Ziwa Booking Accepted successfully.").show();
                setTimeout(function(){ window.location = "/members/bookings"; }, 1500);
            }).fail(function () {
                $("#mws_booking_form_msg").removeClass("success").addClass('error').html("Ziwa Booking Accepted error were encountered if problem persists please email info@ziwatours.biz.").show();
            });
        });

        $("#btn_cancel_booking").click(function(evt){
            evt.preventDefault();
            var booking_id = $(this).data("book-id");
            var form_data = [];
            form_data.push({ name: "booking_id", value: booking_id});
            $.post('/members/cancelBooking', form_data, function (data) {
                $("#mws_booking_form_msg").removeClass("error").addClass('success').html("SUCCESS : Ziwa Booking Cenceled sucessfully.").show();
                setTimeout(function(){ window.location = "/members/bookings"; }, 1500);
            }).fail(function () {
                $("#mws_booking_form_msg").removeClass("success").addClass('error').html("ERROR : Ziwa Booking Cencel error were encountered if problem persists please email info@ziwatours.biz..").show();
            });
        });

        $("#btn_member_enquiry").click(function(evt){
            evt.preventDefault();
            var form_data = $("#frm_member_enquiry").serializeArray();


            if ($.trim($("#enquiry_body").val())) {
                $.post('/members/sendMessage', form_data, function (data) {
                    if (data != "succes") {
                        $("#mws_enquiry_form_msg").removeClass("error").addClass('success').html("Ziwa Message was sent successfully.");
                        $("#mws_enquiry_form_msg").show();
                        $('#frm_member_enquiry').trigger("reset");
                    }
                    else {
                        $("#mws_enquiry_form_msg").removeClass("success").addClass('error').html("Data Error Ziwa Message was not sent successfully if problem persists please email info@ziwatours.biz.");
                        $("#mws_enquiry_form_msg").show();
                    }
                }).fail(function () {
                    $("#mws_enquiry_form_msg").removeClass("success").addClass('error').html("Coonection Error Ziwa Message was not sent successfully if problem persists please email info@ziwatours.biz.");
                    $("#mws_enquiry_form_msg").show();
                });
            }
            else
            {
                $("#mws_enquiry_form_msg").removeClass("success").addClass('error').html("Validation Error Ziwa Message was not sent successfully if problem persists please email info@ziwatours.biz.");
                $("#mws_enquiry_form_msg").show();

            }
        });

        $("#memeber_save_profile").click(function(evt){
            evt.preventDefault();
            var form_data = $("#frm_user_profile").serializeArray();
            //console.log(form_data);
            $.post('/members/saveUserProfile', form_data, function(data){
                if(data != "succes") {
                    $("#user_profile_mws-validate-error").removeClass("error").addClass('success').html("Profile information was saved successfully.");
                    $("#user_profile_mws-validate-error").show();
                }
                else {
                    $("#user_profile_mws-validate-error").removeClass("success").addClass('error').html("Data Error Profile information was not saved successfully if problem persists please email info@ziwatours.biz.");
                    $("#user_profile_mws-validate-error").show();
                }
            }).fail(function() {
                $("#user_profile_mws-validate-error").removeClass("success").addClass('error').html("Coonection Error Profile information was not saved successfully if problem persists please email info@ziwatours.biz.");
                $("#user_profile_mws-validate-error").show();
            });
        });

        $("#memeber_save_password").click(function(evt){
            evt.preventDefault();
            if($("#opass").val().trim().length < 3) {
                $("#mws-password-validate-error").removeClass("success").addClass('error').html("Please enter a password before trying to save your password.");
                $("#mws-password-validate-error").show();
            }
            else
            {
                var form_data = [];
                form_data.push({ name: "use_id", value: $("#password_user_id").val()});
                form_data.push({ name: "use_password", value: $("#opass").val().trim()});
                $.post('/members/changeUserPassword', form_data, function(data){
                    if(data != "succes") {
                        $("#mws-password-validate-error").removeClass("error").addClass('success').html("Password was saved successfully.");
                        $("#mws-password-validate-error").show();
                        $('#frm_user_password').trigger("reset");
                    }
                    else {
                        $("#mws-password-validate-error").removeClass("success").addClass('error').html("Data Error password was not saved successfully if problem persists please email info@ziwatours.biz.");
                        $("#mws-password-validate-error").show();
                    }
                }).fail(function() {
                    $("#mws-password-validate-error").removeClass("success").addClass('error').html("Coonection Error password was not saved successfully if problem persists please email info@ziwatours.biz.");
                    $("#mws-password-validate-error").show();
                });

            }

        });



    });


}) (jQuery, window, document);